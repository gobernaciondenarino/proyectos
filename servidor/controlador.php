<?php
require_once dirname(__FILE__) . '/conexion.php';
require_once dirname(__FILE__) . '/sesion.php';
require_once dirname(__FILE__) . '/Log.php';
//error_reporting(E_ALL);
//error_reporting(E_ALL ^ E_NOTICE);
//error_reporting(0);
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
if ($_POST) {
	

    if (isset($_POST['id_formulario'])) {
        $typeForm = $_POST['id_formulario'];
        switch ($typeForm) {
            case 'login':
            loguearUsuario();
            break;

            case 'unlogin':
            cerrarSesion();
            header("Location: ./../");
            break;

            case 'activarInactivarUsuario':
            activarInactivarUsuario();
            break;

            case 'cargarListadoUsuarios':
            cargarListadoUsuarios();
            break;

            case 'registrarUsuario':
            registrarUsuario();
            break;

            case 'buscarUsuario':
            buscarUsuario();
            break; 

            case 'editarUsuario':
            editarUsuario();
            break; 



            case 'cargarListadoProyectos':
            cargarListadoProyectos();
            break;

            case 'registrarProyecto':
            registrarProyecto();
            break;

            case 'buscarProyecto':
            buscarProyecto();
            break; 

            case 'actualizarProyecto':
            actualizarProyecto();
            break;

            case 'actualizarVersion':
            actualizarVersion();
            break;

            case 'buscarVersion':
            buscarVersion();
            break;

            case 'activarInactivarProyecto':
            activarInactivarProyecto();
            break;



            case 'cargarListadoConfiguraciones':
            cargarListadoConfiguraciones();
            break;

            case 'registrarEstado':
            registrarEstado();
            break;

            case 'registrarModelo':
            registrarModelo();
            break;

            case 'editarEstado':
            editarEstado();
            break;

            case 'editarModelo':
            editarModelo();
            break;



            case 'cambiarContrasena':
            cambiarContrasena();
            break;

        }
    }
}

function cambiarContrasena(){
    try{
        $id_usuario = Controlador::darIdUsuario();
        $pdo = Conexion::singleton();
        $password = $_POST['txt_password'];
        $pdo->cambiarContrasena($password , $id_usuario);
        echo '{"status":1 ,  "msg": "Contraseña actualizada correctamente"}';

    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite activar o desactivar un proyecto
function activarInactivarProyecto(){
    try{
        $id_proyecto = $_POST['id_proyecto'];
        $valor = $_POST['valor'];
        $pdo = Conexion::singleton();
        $pdo->activarInactivarProyecto($id_proyecto, $valor);
        echo '{"status":1 ,  "msg": "Proyecto actualizado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
    }
}

// Permite editar el nombre de un modelo
function editarModelo(){
    try{
        $id_modelo = $_POST['id_modelo'];
        $modelo = $_POST['txt_modelo'];
        $pdo = Conexion::singleton();
        $pdo->editarModelo($id_modelo, $modelo);
        echo '{"status":1 ,  "msg": "Modelo actualizado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite editar el nombre de un modelo
function editarEstado(){
    try{
        $id_estado = $_POST['id_estado'];
        $estado = $_POST['txt_estado'];
        $estado_inicial = isset($_POST['cbxe_estado_inicial']) && $_POST['cbxe_estado_inicial'] ? 1 : 0;
        $pdo = Conexion::singleton();
        $pdo->editarEstado($id_estado, $estado, $estado_inicial);
        echo '{"status":1 ,  "msg": "Estado actualizado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite registrar un modelo
function registrarModelo(){
    try{
        $modelo = $_POST['txt_modelo'];
        $pdo = Conexion::singleton();
        $pdo->registrarModelo($modelo);
        echo '{"status":1 ,  "msg": "Modelo creado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite registrar un estado
function registrarEstado(){
    try{
        $estado = $_POST['txt_estado'];
        $estado_inicial = isset($_POST['cbx_estado_inicial']) && $_POST['cbx_estado_inicial'] ? 1 : 0;
        $pdo = Conexion::singleton();
        $pdo->registrarEstado($estado , $estado_inicial);
        echo '{"status":1 ,  "msg": "Estado creado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite cargar la lista de configuraciones 
function cargarListadoConfiguraciones(){
    try{
        $pdo = Conexion::singleton();
        $estados = $pdo->darEstados();
        $modelos = $pdo->darModelosContratacion();
        $res =  '{"status":1 ,  
        "estados": [';
        for ($i=0; $i < count($estados) ; $i++) { 
            $res .= '{
            "id_estado":'.$estados[$i]->id.',
            "estado_inicial":'.$estados[$i]->estado_inicial.',
            "estado":"'.$estados[$i]->estado.'"
            }';
            $res .= ($i + 1) < count($estados) ? ' , ' : '';
        }

        $res .= '], "modelos": [';
        for ($i=0; $i < count($modelos) ; $i++) { 
            $res .= '{
            "id_modelo":'.$modelos[$i]->id.',
            "modelo":"'.$modelos[$i]->modelo.'"
            }';
            $res .= ($i + 1) < count($modelos) ? ' , ' : '';
        }
        $res .= ']}';
        echo $res;
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite buscar una version del proyecto
function buscarVersion(){
    try{
        $id_version = $_POST['id_version'];
        $pdo = Conexion::singleton();
        $version = $pdo->buscarVersion($id_version);
        $version = $version[0];
        $archivos = $pdo->darArchivosVersion($version->id);
        $fecha_respuesta = $version->fecha_respuesta;
        if($fecha_respuesta === null){
             $fecha_respuesta = $pdo->darFechaDB();
        }
        $res =  '{"status":1 ,  
        "id_version": '.$version->id.',
        "obs_juridica": "'.$version->obs_juridica.'",
        "obs_tecnica": "'.$version->obs_tecnica.'",
        "fecha_envio_uv": "'.$version->fecha_envio.'",
        "fecha_respuesta_uv": "'.$fecha_respuesta.'",
        "ultima_version": '.$version->numero.',
        "archivos":[';
        for ($i=0; $i < count($archivos); $i++) { 
            $a = $archivos[$i];
            $res .= '{
                "ruta_descarga": "servidor/files/'.$a->nombre_ruta.'",
                "nombre_archivo": "'.$a->nombre_archivo.'",
                "nombre_ruta": "'.$a->nombre_ruta.'"
            }';
            $res .= ($i + 1) < count($archivos) ? ' , ' : '';
        }
        $res .= ']';
        $res .= '}';
        echo $res;
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite actualizar la version del proyecto
function actualizarVersion(){
    try{
        $id_version = $_POST['id_version'];
        $txt_obs_tec = $_POST['txte_obs_tec'];
        $txt_obs_jur = $_POST['txte_obs_jur'];       
        $pdo = Conexion::singleton();
        $id_usuario = Controlador::darIdUsuario();

        $fecha_respuesta = $_POST['txte_fecha_respuesta_uv'];
        if($fecha_respuesta == '' || $fecha_respuesta == null){
            $fecha_respuesta = null;
        }else{
            if(substr_count($fecha_respuesta, ':') === 1){
                $fecha_respuesta = $fecha_respuesta.':00';
            }
            
        }

        
        //$date = strtotime($fecha_respuesta);
        //$fecha_respuesta  = date('Y-M-d H:i', $date);
        if($fecha_respuesta == null){
            $fecha_respuesta = $pdo->darFechaDB();
        }

        
        $pdo->actualizarVersion($id_version, $txt_obs_tec, $txt_obs_jur, $fecha_respuesta, $id_usuario);
        echo '{"status":1 ,  "msg": "Version actualizada correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite actualizar un prpoyecto
function actualizarProyecto(){
    try{
        $id_proyecto = $_POST['id_proyecto'];
        $nombre = $_POST['txte_nombre'];
        $id_modelo = $_POST['slte_modelo_contratacion'];
        $id_estado = $_POST['slte_estado'];
        $id_usuario = Controlador::darIdUsuario();
        
        $info_archivos = cargarArchivo($_FILES);

        $pdo = Conexion::singleton();
        $fecha_ultima_modificacion = $pdo->darFechaDB();
        $pdo->actualizarProyecto($id_proyecto, $nombre, $id_modelo, $id_estado , $info_archivos, $fecha_ultima_modificacion, $id_usuario);
        echo '{"status":1 ,  "msg": "Proyecto actualizado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite buscar un proyecto
function buscarProyecto(){
    try{
        $id_proyecto = $_POST['id_proyecto'];

        $pdo = Conexion::singleton();
        $proyecto = $pdo->buscarProyecto($id_proyecto);
        $versiones = $pdo->darVersionesProyecto($id_proyecto);
        if(count($proyecto) > 0){
            $proyecto = $proyecto[0];
            $res =  '{"status":1 ,  
            "id_proyecto": '.$proyecto->id.',
            "nombre": "'.$proyecto->nombre.'",
            "creado_por": "'.$proyecto->creado_por.'",
            "fecha_radicacion": "'.$proyecto->fecha_radicacion.'",
            "fecha_ultima_modificacion": "'.$proyecto->fecha_ultima_modificacion.'",
            "id_modelo": '.$proyecto->id_modelo.',
            "activo": '.$proyecto->activo.',
            "id_estado": '.$proyecto->id_estado;
            if(count($versiones)> 0){
                $uv = $versiones[0];
                $archivos = $pdo->darArchivosVersion($uv->id);
                $fecha_respuesta = $uv->fecha_respuesta;
                if($fecha_respuesta === null){
                     $fecha_respuesta = $pdo->darFechaDB();
                     $date = strtotime($fecha_respuesta);
                     $fecha_respuesta  = date('Y-m-d H:i', $date);
                }
                $res .= ',
                "id_version": "'.$uv->id.'",
                "obs_juridica": "'.$uv->obs_juridica.'",
                "obs_tecnica": "'.$uv->obs_tecnica.'",
                "fecha_envio_uv": "'.$uv->fecha_envio.'",
                "fecha_respuesta_uv": "'.$fecha_respuesta.'",
                "ultima_version": '.$uv->numero.',
                "archivos":[';
                for ($i=0; $i < count($archivos); $i++) { 
                    $a = $archivos[$i];
                    $res .= '{
                        "ruta_descarga": "servidor/files/'.$a->nombre_ruta.'",
                        "nombre_archivo": "'.$a->nombre_archivo.'",
                        "nombre_ruta": "'.$a->nombre_ruta.'"
                    }';
                    $res .= ($i + 1) < count($archivos) ? ' , ' : '';
                }
                $res .= '],';

                $res.= '"versiones":[';
                for ($i=0; $i < count($versiones); $i++) { 
                    $v = $versiones[$i];
                    $archivos = $pdo->darArchivosVersion($v->id);
                    $res .= '{
                        "id_version": "'.$v->id.'",
                        "modificado_por": "'.$v->modificado_por.'",
                        "fecha_envio": "'.$v->fecha_envio.'",
                        "fecha_respuesta": "'.$v->fecha_respuesta.'",
                        "numero": '.$v->numero.',
                        "archivos":[';
                        for ($ii=0; $ii < count($archivos); $ii++) { 
                            $a = $archivos[$ii];
                            $res .= '{
                                "ruta_descarga": "servidor/files/'.$a->nombre_ruta.'",
                                "nombre_archivo": "'.$a->nombre_archivo.'",
                                "nombre_ruta": "'.$a->nombre_ruta.'"
                            }';
                            $res .= ($ii + 1) < count($archivos) ? ' , ' : '';
                        }
                        $res .= ']';
                    $res .= '}';
                    $res .= ($i + 1) < count($versiones) ? ' , ' : '';
                }
                $res .= ']';
            }
            $res .= '}';
            echo $res;
        }
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite registrar un proyecto
function registrarProyecto(){
    try{
        $nombre = $_POST['txt_nombre'];
        $id_modelo = $_POST['slt_modelo_contratacion'];
        $id_estado = $_POST['slt_estado'];
        $id_usuario = Controlador::darIdUsuario();
        $info_archivos = cargarArchivo($_FILES);

        $pdo = Conexion::singleton();
        $pdo->registrarProyecto($nombre, $id_modelo, $id_estado , $id_usuario , $info_archivos);
        echo '{"status":1 ,  "msg": "Proyecto creado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite cargar un archivo de un proyecto
function cargarArchivo($files) {
    $archivos = array();
    $informacionArchivo = null;
    $nombreInputFile = "inputFile";
    if (isset($files[$nombreInputFile])) {
        $file = $files[$nombreInputFile];
        for($i = 0 ; $i < count($file['name']); $i++){
            $tamanioArchivo = $file['size'][$i];
            $tipoArchivo = $file['type'][$i];
            $nombreArchivo = basename($file['name'][$i]);
            $soloNombre = explode("." , $nombreArchivo);
            $ext = pathinfo($nombreArchivo, PATHINFO_EXTENSION);
            $nombreArchivoEnServidor = $soloNombre[0].'_'.(round(microtime(true) * 1000)) . '.' . $ext;
            $rutaMoverArchivo = getPathFile($nombreArchivoEnServidor);
            if (move_uploaded_file($file['tmp_name'][$i], $rutaMoverArchivo)) {
                $informacionArchivo= array('nombreArchivo' => $nombreArchivo, 'tamanioArchivo' => $tamanioArchivo, 'tipoArchivo' => $tipoArchivo, 'nombreArchivoEnServidor' => $nombreArchivoEnServidor);
                array_push($archivos, $informacionArchivo);
            }
        }
       
    }
    return $archivos;
}

function getPathFile($nombreArchivo) {
    return substr($_SERVER["SCRIPT_FILENAME"], 0, strrpos($_SERVER["SCRIPT_FILENAME"], "/")) . "/files/" . $nombreArchivo;
}

// Permite cargar la lista de los proyecto creados
function cargarListadoProyectos(){
    try {
        $busqueda = $_POST['txt_busqueda'];
        $id_modelo = $_POST['slt_modelo_c'];
        $id_estado = $_POST['slt_estado'];
        $id_usuario = $_POST['slt_usuario'];
        $pdo = Conexion::singleton();
        $proyectos = $pdo->consultarProyectos($busqueda, $id_modelo, $id_estado, $id_usuario);
        $res = '{"status":1 ,
        "nivel_permiso": '.Controlador::darNivelPermiso().' ,  
        "proyectos": [';
        for ($i=0; $i < count($proyectos); $i++) { 
            $res .= '{'
            . '"id":' . $proyectos[$i]->id . ','
            . '"nombre":"' . $proyectos[$i]->nombre . '",'
            . '"modelo":"' . $proyectos[$i]->modelo . '",'
            . '"estado":"' . $proyectos[$i]->estado . '",'
            . '"activo":'.  $proyectos[$i]->activo . ','
            . '"creado_por":"' . $proyectos[$i]->creado_por . '",'
            . '"fecha_radicacion":"' . $proyectos[$i]->fecha_radicacion . '",'
            . '"fecha_ultima_modificacion":"' . $proyectos[$i]->fecha_ultima_modificacion . '"'
            . '}';
            $res .= ($i + 1) < count($proyectos) ? ' , ' : '';
        }
        $res .="]}";
        echo $res;
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite editar usuario
function editarUsuario(){
 try{
    $id_usuario = $_POST['id_usuario'];
    $nombres = $_POST['txte_nombres'];
    $apellidos = $_POST['txte_apellidos'];
    $password = $_POST['txte_password'];
    $activo = isset($_POST['cbxe_activo']) && $_POST['cbxe_activo'] ? 1 : 0;
    $id_rol = $_POST['slte_rol'];

    $pdo = Conexion::singleton();
    $pdo->editarUsuario($id_usuario, $nombres, $apellidos , $password , $activo , $id_rol);
    echo '{"status":1 ,  "msg": "Usuario actualizado correctamente"}';
} catch (Exception $e) {
    echo '{"status":0 , "msg":"Error"}';
    Log::write($e->getMessage());
}
}

// Permite buscar usuario
function buscarUsuario(){
    try{
        $id_usuario = $_POST['id_usuario'];

        $pdo = Conexion::singleton();
        $usuario = $pdo->buscarUsuario($id_usuario);
        if(count($usuario) > 0){
            $usuario = $usuario[0];
            $res =  '{"status":1 ,  
            "id": "'.$usuario->id.'",
            "nombres": "'.$usuario->nombres.'",
            "apellidos": "'.$usuario->apellidos.'",
            "username": "'.$usuario->username.'",
            "activo": '.$usuario->activo.',
            "id_rol": '.$usuario->id_rol.'
        }';
        echo $res;
    }
} catch (Exception $e) {
    echo '{"status":0 , "msg":"Error"}';
    Log::write($e->getMessage());
}
}

// Permite registrar usuario
function registrarUsuario(){
    try{
        $username = $_POST['txt_username'];
        $pdo = Conexion::singleton();
        $usuarios = $pdo->existeUsuario($username);
        if(count($usuarios) > 0){
            echo '{"status":0 , "msg":"El usuario: \"'.$username.'\" , ya se encuentra registrado."}';
        }else{
            $nombres = $_POST['txt_nombres'];
            $apellidos = $_POST['txt_apellidos'];
            
            $password = $_POST['txt_password'];
            $activo = isset($_POST['cbx_activo']) && $_POST['cbx_activo'] ? 1 : 0;
            $id_rol = $_POST['slt_rol'];

            $pdo->registrarUsuario($nombres, $apellidos, $username , $password , $activo , $id_rol);
            echo '{"status":1 ,  "msg": "Usuario registrado correctamente"}';
        }
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
        Log::write($e->getMessage());
    }
}

// Permite cargar la lista de usuario
function cargarListadoUsuarios(){
    try {
        $usuarios = Controlador::darUsuarios();
        $res = '{"status":1 ,  "usuarios": [';
        for ($i=0; $i < count($usuarios); $i++) { 
            $res .= '{'
            . '"id":' . $usuarios[$i]->id . ','
            . '"nombres":"' . $usuarios[$i]->nombres . '",'
            . '"apellidos":"' . $usuarios[$i]->apellidos . '",'
            . '"username":"' . $usuarios[$i]->username . '",'
            . '"rol":"' . $usuarios[$i]->rol . '",'
            . '"activo":' . $usuarios[$i]->activo 
            . '}';
            $res .= ($i + 1) < count($usuarios) ? ' , ' : '';
        }
        $res .="]}";
        echo $res;
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
    }
}

// Permite activar o desactivar usuarios
function activarInactivarUsuario(){
    try{
        $id_usuario = $_POST['id_usuario'];
        $valor = $_POST['valor'];
        Controlador::activarInactivarUsuario($id_usuario, $valor);
        echo '{"status":1 ,  "msg": "Usuario actualizado correctamente"}';
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
    }
}

// Permite loguear usuario
function loguearUsuario() {
    try {
        if (!estaLogueado()) {
            $user = strtolower($_POST['username']);
            $pass = $_POST['password'];
            $msg = login($user, $pass);
            echo $msg;
        }
    } catch (Exception $e) {
        echo '{"status":0 , "msg":"Error"}';
    }
}



// Clase del controlador
class Controlador{

	// Niveles de usuario
    public static $NIVEL_ADMINISTRADOR = 1;
    public static $NIVEL_ABOGADO = 2;
    public static $NIVEL_CONSULTA = 3;

    // Retorna ID del usuario
    public static function darIdUsuario(){
        try{
            return darIdUsuario();     
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    // Retorna rol del usuario
    public static function darRoles() {
      try {
         $pdo = Conexion::singleton();
         return $pdo->darRoles();
     } catch (Exception $e) {
         echo $e->getMessage();
     }
 }

// Retorna usuarios
 public static function darUsuarios() {
  try {
     $pdo = Conexion::singleton();
     return $pdo->darUsuarios();
 } catch (Exception $e) {
     echo $e->getMessage();
 }
}

// Retorna Modelo de contratacion de los proyectos
public static function darModelosContratacion() {
    try {
        $pdo = Conexion::singleton();
        return $pdo->darModelosContratacion();
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

// Retorna Estados de los proyectos
public static function darEstados() {
    try {
        $pdo = Conexion::singleton();
        return $pdo->darEstados();
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

// Retorna estado del usuario logueado
public static function usuarioEstaLogueado(){
  return estaLogueado();
}

// Retorna informacion del usuario loqueado
public static function darInformacionUsuario(){
    try{
        return darNombreUsuario() . ' (' . darRolUsuario() . ')' ;     
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

// Retorna nombre del usuario
public static function darNombreCompletoUsuario(){
    try{
        return darNombreUsuario() .' '. darApellidoUsuario();     
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

// Retorna esado de usuario
public static function activarInactivarUsuario($id_usuario, $valor){
    try {
        $pdo = Conexion::singleton();
        return $pdo->activarInactivarUsuario($id_usuario, $valor);
    } catch (Exception $e) {
        echo $e->getMessage();
        Log::write($e->getMessage());
    }
}

// Retorna usuarios registrados
public static function registrarUsuario($nombres, $apellidos, $username , $password , $activo , $id_rol){
    try {
        $pdo = Conexion::singleton();
        return $pdo->registrarUsuario($nombres, $apellidos, $username , $password , $activo , $id_rol);
    } catch (Exception $e) {
        echo $e->getMessage();
        Log::write($e->getMessage());
    }
}

// Retorna nivel de permiso del usuario
public static function darNivelPermiso(){
    try{
        return darNivelPermiso();     
    } catch (Exception $e) {
        echo $e->getMessage();
        Log::write($e->getMessage());
    }
}

}

?>