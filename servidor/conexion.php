<?php 
require_once dirname(__FILE__) . '/Log.php';
class Conexion {

	// Datos de conexion con la base de datos
    private static $instancia;
    private $dbh;
    private $dbname = "control_proyectos";
    private $host = "localhost";
    private $user = "root";
    private $pass = 'toor';
    private $port = 3306;

    private function __construct() {
        try {
            $this->dbh = new PDO("mysql:host=$this->host;port=$this->port;dbname=$this->dbname", $this->user, $this->pass);
            $this->dbh->exec("SET client_encoding = 'UTF8';");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (Exception $e) {
            echo $e->getMessage();
            throw new Exception($e->getMessage());
        }
    }

    public static function singleton() {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    // Consultas en la base de datos
    public function consultarQuery($queryIn) {


       // $query = $this->dbh->prepare('DELETE FROM usuario WHERE id != 1 ');
        //$query = $this->dbh->prepare('UPDATE usuario SET id_rol = 2 WHERE id != 1 ');
        //$query->execute();
        
        $query = $this->dbh->prepare($queryIn);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }



    // Retorna roles de la base de datos
    public function darRoles() {
        return $this->consultarQuery('SELECT * FROM rol ORDER BY nombre ASC');
    }

	// Retorna usuarios de la base de datos
    public function darUsuarios() {
        return $this->consultarQuery('SELECT u.* , r.nombre AS "rol" FROM usuario u , rol r WHERE u.id_rol = r.id ORDER BY u.nombres ASC');
    }

    // Retorna modelos de contratacion de la base de datos
    public function darModelosContratacion(){
        return $this->consultarQuery('SELECT * FROM modelo_contratacion ORDER BY modelo ASC');   
    }

    // Retorna estados de la base de datos
    public function darEstados(){
        return $this->consultarQuery('SELECT * FROM estado ORDER BY estado ASC');   
    }

    // Retorna usuario y passwrod de la base de datos
    public function login($user, $pass) {
        try {
            $query = $this->dbh->prepare('SELECT u.* , r.nombre AS "rol", r.nivel_permiso FROM usuario u, rol r WHERE u.username = ? AND u.password = MD5( ? ) AND u.id_rol = r.id');
            $query->bindParam(1, $user);
            $query->bindParam(2, $pass);
            $query->execute();
            return $query->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Retorna estado del usuario
    public function activarInactivarUsuario($id_usuario, $valor){
        try {
            $query = $this->dbh->prepare('UPDATE usuario SET activo = ? WHERE id = ? ');
            $query->bindParam(1, $valor);
            $query->bindParam(2, $id_usuario);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Registra usuario en la base de datos
    public function registrarUsuario($nombres, $apellidos, $username , $password , $activo , $id_rol){
        try {
            $query = $this->dbh->prepare('INSERT INTO usuario(nombres, apellidos, username, password, activo, id_rol) VALUES (?,?,?,MD5(?),?,?);');
            $query->bindParam(1, $nombres);
            $query->bindParam(2, $apellidos);
            $query->bindParam(3, $username);
            $query->bindParam(4, $password);
            $query->bindParam(5, $activo);
            $query->bindParam(6, $id_rol);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	// Busca usuarios en la base de datos
    public function buscarUsuario($id_usuario) {
        try {
            $query = $this->dbh->prepare('SELECT * FROM usuario WHERE id = ?');
            $query->bindParam(1, $id_usuario);
            $query->execute();
            return $query->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

     // Edita usuarios en la base de datos
     public function editarUsuario($id_usuario, $nombres, $apellidos , $password , $activo , $id_rol){
        try {
            $q = '';
            if (!empty($password)) {
                $q = ' ,  password =  MD5('.$password.') ';
            }
            $query = $this->dbh->prepare('UPDATE usuario SET nombres = ? , apellidos = ? , activo = ? , id_rol = ? '. $q .' WHERE id = ? ');
            $query->bindParam(1, $nombres);
            $query->bindParam(2, $apellidos);
            $query->bindParam(3, $activo);
            $query->bindParam(4, $id_rol);
            $query->bindParam(5, $id_usuario);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Consulta proyectos en la base de datos
    public function consultarProyectos($busqueda , $id_modelo, $id_estado,  $id_usuario){

        $q = '';
        if (!empty($busqueda)) {
            $q = ' AND LOWER(CONCAT(p.id, p.nombre , u.nombres, u.apellidos , e.estado , t.modelo)) LIKE LOWER(\'%' . $busqueda . '%\') ';
        }
        if($id_modelo > 0){
            $q .= ' AND t.id = ' . $id_modelo . ' ';   
        }
        if($id_estado > 0){
            $q .= ' AND e.id = ' . $id_estado . ' ';   
        }

        if($id_usuario > 0){
            $q .= ' AND u.id = ' . $id_usuario . ' ';   
        }

        return $this->consultarQuery('SELECT p.* , CONCAT(u.nombres, " " , u.apellidos) AS "creado_por" , t.modelo AS "modelo" , e.estado AS "estado" FROM proyecto p , usuario u , estado e , modelo_contratacion t WHERE u.id = p.id_usuario AND t.id = p.id_modelo AND e.id = p.id_estado '.$q.' ORDER BY p.fecha_radicacion DESC');
    }

    // Registra proyectos en la base de datos
    public function registrarProyecto($nombre, $id_modelo, $id_estado , $id_usuario , $info_archivos){
        try {
            $query = $this->dbh->prepare('INSERT INTO proyecto(nombre, id_modelo, id_estado, id_usuario) VALUES (?,?,?,?);');
            $query->bindParam(1, $nombre);
            $query->bindParam(2, $id_modelo);
            $query->bindParam(3, $id_estado);
            $query->bindParam(4, $id_usuario);
            $query->execute();
            $id_proyecto = $this->dbh->lastInsertId();
            $versiones = $this->darVersionProyecto($id_proyecto);
            $numero_version = 1 ;
            if(count($versiones) > 0){
                $numero_version = ($versiones[0]->numero) + 1 ;
            }

            

            if(count($info_archivos) > 0){
                $id_version = $this->registrarVersion( $id_proyecto, $numero_version, $id_usuario);
                for($i = 0 ; $i < count($info_archivos); $i++){
                    $info_archivo = $info_archivos[$i];
                    $this->registrarArchivo($info_archivo['nombreArchivo'], $info_archivo['nombreArchivoEnServidor'] , $id_version);
                    
                }
                
            }
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Registra version del proyectos en la base de datos
    public function registrarVersion( $id_proyecto, $numero, $id_usuario){
        try {
            $query = $this->dbh->prepare('INSERT INTO version(id_proyecto,numero, id_usuario, fecha_respuesta) VALUES (?,?,?,?);');
            $query->bindParam(1, $id_proyecto);
            $query->bindParam(2, $numero);
            $query->bindParam(3, $id_usuario);
            $fecha = $this->darFechaDB();
            $query->bindParam(4, $fecha);

            $query->execute();
            $id_version = $this->dbh->lastInsertId();
            return $id_version;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	// Registra archivo del proyectos en la base de datos
    public function registrarArchivo($nombre, $ruta, $id_version){
        try {
            $query = $this->dbh->prepare('INSERT INTO archivo(nombre, ruta, id_version) VALUES (?,? ,?);');
            $query->bindParam(1, $nombre);
            $query->bindParam(2, $ruta);
            $query->bindParam(3, $id_version);
            $query->execute();
            $id_archivo = $this->dbh->lastInsertId();
            return $id_archivo;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Retorna versiones de los proyectos de la base de datos
    public function darVersionProyecto($id_proyecto) {
        return $this->consultarQuery('SELECT v.numero FROM version v WHERE v.id_proyecto = '.$id_proyecto.' ORDER BY v.numero DESC');
    }

    // Busca proyectos en la base de datos
    public function buscarProyecto($id_proyecto) {
        try {
            $query = $this->dbh->prepare('
                SELECT p.* , CONCAT(u.nombres, " " , u.apellidos) AS "creado_por" , t.id AS "id_modelo" , e.id AS "id_estado" 
                FROM proyecto p , usuario u , estado e , modelo_contratacion t 
                WHERE u.id = p.id_usuario AND t.id = p.id_modelo AND e.id = p.id_estado AND p.id = '.$id_proyecto.' ORDER BY p.fecha_radicacion DESC');
            $query->bindParam(1, $id_proyecto);
            $query->execute();
            return $query->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Retorna versiones de los proyectos de la base de datos
    public function darVersionesProyecto($id_proyecto) {
        return $this->consultarQuery('SELECT v.*  , CONCAT(u.nombres  , " " , u.apellidos ) AS "modificado_por" 
            FROM proyecto p, version v, usuario u
            WHERE p.id = '.$id_proyecto.' AND v.id_proyecto = p.id AND v.id_usuario = u.id ORDER BY v.numero DESC');
    }

    // Retorna versiones de los proyectos de la base de datos
    public function darArchivosVersion($id_version) {
        return $this->consultarQuery('SELECT a.nombre AS "nombre_archivo" , a.ruta AS "nombre_ruta" 
            FROM archivo a WHERE a.id_version = '.$id_version.' ORDER BY a.nombre ASC');
    }

    // Actualiza proyectos en la base de datos
    public function actualizarProyecto($id_proyecto, $nombre, $id_modelo, $id_estado , $info_archivos, $fecha_ultima_modificacion, $id_usuario){
        try {
            $query = $this->dbh->prepare('UPDATE proyecto SET nombre = ? , id_modelo = ?, id_estado = ? , fecha_ultima_modificacion = ? WHERE id = ? ;');
            $query->bindParam(1, $nombre);
            $query->bindParam(2, $id_modelo);
            $query->bindParam(3, $id_estado);
            $query->bindParam(4, $fecha_ultima_modificacion);
            $query->bindParam(5, $id_proyecto);
            $query->execute();
            $versiones = $this->darVersionProyecto($id_proyecto);
            $numero_version = 1 ;
            if(count($versiones) > 0){
                $numero_version = ($versiones[0]->numero) + 1 ;
            }

            if(count($info_archivos) > 0){
                $id_version = $this->registrarVersion( $id_proyecto, $numero_version, $id_usuario);
                for($i = 0 ; $i < count($info_archivos); $i++){
                    $info_archivo = $info_archivos[$i];
                    $this->registrarArchivo($info_archivo['nombreArchivo'], $info_archivo['nombreArchivoEnServidor'] , $id_version);
                    
                }
                
            }
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Actualiza versiones de los proyectos en la base de datos
    public function actualizarVersion($id_version, $txt_obs_tec, $txt_obs_jur, $fecha_respuesta, $id_usuario){
        try {
            $query = $this->dbh->prepare('UPDATE version SET obs_tecnica = ? , obs_juridica = ?, fecha_respuesta = ? , id_usuario = ?  WHERE id = ? ;');
            $query->bindParam(1, $txt_obs_tec);
            $query->bindParam(2, $txt_obs_jur);
            $query->bindParam(3, $fecha_respuesta);
            $query->bindParam(4, $id_usuario);
            $query->bindParam(5, $id_version);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	// Retorna la fecha de la base de datos
    public function darFechaDB() {
        try {
            $query = $this->dbh->prepare('SELECT DATE_FORMAT(SYSDATE() , \'%Y-%m-%d %h:%i:%s\' ) AS "date_db";');
            $query->execute();
            $date = $query->fetchAll(PDO::FETCH_OBJ);
            return $date[0]->date_db;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Busca versiones de los proyectos en la base de datos
    public function buscarVersion($id_version) {
        try {
            $query = $this->dbh->prepare('
                SELECT v.*   
                FROM version v
                WHERE v.id = '.$id_version.' ORDER BY v.numero DESC');
            $query->bindParam(1, $id_version);
            $query->execute();
            return $query->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Registra estados de los proyectos en la base de datos
    public function registrarEstado($estado, $estado_inicial){
        try {
            if($estado_inicial == 1){
                $query = $this->dbh->prepare('UPDATE estado SET estado_inicial = 0 ;');
                $query->execute();
            }
            $query = $this->dbh->prepare('INSERT INTO estado(estado , estado_inicial) VALUES ( ? , ? ) ;');
            $query->bindParam(1, $estado);
            $query->bindParam(2, $estado_inicial);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	// Edita estados de los proyectos en la base de datos
    public function editarEstado($id_estado, $estado, $estado_inicial){
        try {
            if($estado_inicial == 1){
                $query = $this->dbh->prepare('UPDATE estado SET estado_inicial = 0 ;');
                $query->execute();
            }
            $query = $this->dbh->prepare('UPDATE estado SET estado = ? , estado_inicial = ? WHERE id= ? ;');
            $query->bindParam(1, $estado);
            $query->bindParam(2, $estado_inicial);
            $query->bindParam(3, $id_estado);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Registra modelos de los proyectos en la base de datos
    public function registrarModelo($modelo){
        try {
            $query = $this->dbh->prepare('INSERT INTO modelo_contratacion(modelo) VALUES (?);');
            $query->bindParam(1, $modelo);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	// Edita modelos de los proyectos en la base de datos
    public function editarModelo($id_modelo, $modelo){
        try {
            $query = $this->dbh->prepare('UPDATE modelo_contratacion SET modelo = ? WHERE id= ? ;');
            $query->bindParam(1, $modelo);
            $query->bindParam(2, $id_modelo);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Permite activar o inactivar un proyecto en la base de datos
    public function activarInactivarProyecto($id_proyecto, $valor){
        try {
            $query = $this->dbh->prepare('UPDATE proyecto SET activo = ? WHERE id = ? ');
            $query->bindParam(1, $valor);
            $query->bindParam(2, $id_proyecto);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Verifica username en la base de datos para impedir usuarios duplicados 
    public function existeUsuario($username) {
        try {
            $query = $this->dbh->prepare('SELECT * FROM usuario WHERE username LIKE ?');
            $query->bindParam(1, $username);
            $query->execute();
            return $query->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    public function cambiarContrasena($password , $id_usuario){
        try {
            $query = $this->dbh->prepare('UPDATE usuario SET password = MD5(?) WHERE id = ? ');
            $query->bindParam(1, $password);
            $query->bindParam(2, $id_usuario);
            $query->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


}

?>