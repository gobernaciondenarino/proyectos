<?php

class Log {

    public static $nameLog = __DIR__ . DIRECTORY_SEPARATOR . "files/log.txt";

    public static function read() {
        if (file_exists(Log::$nameLog)) {
            $file = fopen(Log::$nameLog, "r");
            while (!feof($file)) {
                echo fgets($file) . "<br />";
            }
            fclose($file);
        } else {
            $file = fopen(Log::$nameLog, "w");
        }
    }

    public static function write($line) {
        if (!file_exists(Log::$nameLog)) {
            $file = fopen(Log::$nameLog, "w");
        }
        $file = fopen(Log::$nameLog, "a");
        //date_default_timezone_set("UTC");
        putenv('TZ=America/Bogota');
        date_default_timezone_set("America/Bogota");
        $line .= '  ' . date("Y-m-d H:i:s", time());
        fwrite($file, $line . PHP_EOL);
        fclose($file);
    }

}
