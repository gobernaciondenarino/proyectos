<?php

session_start();
require_once dirname(__FILE__) . '/conexion.php';

function estaLogueado() {
    if(isset($_SESSION['user']) && !empty($_SESSION['user'])){
        if(time() < $_SESSION['exp']){
            return true;
        }else{
            session_unset();
            session_destroy();
        }
    }
    return false;
}

// Cierra sesion de un usuario
function cerrarSesion(){
    if(estaLogueado()){
        session_destroy();
    }
}

// Retorna ID de un usuario
function darIdUsuario() {
    if (estaLogueado()) {
        return $_SESSION['id'];
    }
    return null;
}

// Retorna nombre de un usuario
function darNombreUsuario(){
    return $_SESSION['nombres'];
}

// Retorna apellidos de un usuario
function darApellidoUsuario(){
    return $_SESSION['apellidos'];
}

// Retorna usuario
function darUsuario(){
    return $_SESSION['user'];
}

// Retorna el nivel de permiso del usuario
function darNivelPermiso(){
    return $_SESSION['nivel_permiso'];
}

// Retorna el rol del usuario
function darRolUsuario(){
    return $_SESSION['rol'];
}

// Mensaje de erro de usuario INACTIVO o CONTRASEÑA INCORRECTA
function login($user, $pass) {
    try {
        if (estaLogueado()) {
            return null;
        } else {
            $pdo = Conexion::singleton();
            $data = $pdo->login($user, $pass);
            $msg = !empty($data) && !empty($data[0]) ? $data[0]->activo == 1 ? null : "Usuario Inactivo!!" : "Usuario o Constraseña incorrectos!!";
            if (empty($msg)) {
                $_SESSION['user'] = $data[0]->username;
                $_SESSION['nombres'] = $data[0]->nombres;
                $_SESSION['apellidos'] = $data[0]->apellidos;
                $_SESSION['id_rol'] = $data[0]->id_rol;
                $_SESSION['id'] = $data[0]->id;
                $_SESSION['exp'] = time() + (240 * 60);
                $_SESSION['activo'] = $data[0]->activo;
                $_SESSION['rol'] = $data[0]->rol;
                $_SESSION['nivel_permiso'] = $data[0]->nivel_permiso;
            }
            return $msg;
        }
    } catch (Exception $e) {
        echo 'Error ' . $e->getMessage();
    }
}