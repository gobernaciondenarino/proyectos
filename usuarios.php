<?php
require_once dirname(__FILE__) . '/servidor/controlador.php';
if (!Controlador::usuarioEstaLogueado() || Controlador::darNivelPermiso() != Controlador::$NIVEL_ADMINISTRADOR) {
	header("Location: ./");
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<title>Usuarios</title>
	<?php include 'head.php'; ?>

</head>

<body id="page-top">

	<?php include 'nav.php'; ?>

	<section></section> 
	
	<div class="container min-alto">


		<!-- Carga la lista de usuarios registrados-->
		<?php include 'alerts.php'; ?>
		<div id="listado_usuarios" class="col-sm-12">
			<div class="row form-group">
				<h3 class="text-left col-sm-11">Usuarios</h3>
			<div class="col-sm-1">
				<button class="btn btn btn-success btn-lg" onclick="habilitarPanel('registrar_usuario')" title="Registrar Nuevo Usuario"><i class="fas fa-plus"></i></button>
			</div>
			</div>
			<div class="col-sm-12" style="overflow: auto; max-height: 400px; width: 100%;">
				<table class="table">
					<thead class="thead-dark">

						<!-- Encabezado de la tabla con atributos de los usuarios -->
						<tr>
							<th scope="col">Nombres</th>
							<th scope="col">Apellidos</th>
							<th scope="col">Username</th>
							<th scope="col">Rol</th>
							<th scope="col">Opciones</th>
						</tr>
					</thead>
					<tbody id="TBODY_LISTADO_USUARIOS">
						
					</tbody>
				</table>
			</div>
		</div>

		<!-- Opcion para registrar un nuevo usuario-->
		<div id="registrar_usuario" class="col-sm-6 offset-sm-3">
			<h3 class="text-center">Registrar Usuario</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_REGISTRAR_USUARIO" onsubmit="registrarUsuario(this.id, event);">
				<input type="hidden" value="registrarUsuario" name="id_formulario">
				<div class="col-sm-12 form-group row">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="inputGroupSelect01"><b>Rol de Usuario</b></label>
						</div>
						<select class="custom-select" name="slt_rol" id="slt_rol" required="true">
							<?php $roles = Controlador::darRoles();
							for ($i = 0; $i < count($roles); $i++) {	?>
							<option value="<?php echo $roles[$i]->id; ?>"><?php echo $roles[$i]->nombre; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<!-- Nombre del nuevo usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_nombres"><b>Nombres</b></label>
					<div class="col-sm-9">
						<input maxlength="100" type="text" class="form-control" id="txt_nombres" name="txt_nombres" placeholder="Digite los nombres" autocomplete="off" required>
					</div>
				</div>

				<!-- Apellidos del nuevo usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_apellidos"><b>Apellidos</b></label>
					<div class="col-sm-9">
						<input maxlength="100" type="text" class="form-control" id="txt_apellidos" name="txt_apellidos" placeholder="Digite los apellidos" autocomplete="off" required>
					</div>
				</div>

				<!-- Username del nuevo usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_username"><b>Username</b></label>
					<div class="col-sm-9">
						<input maxlength="20" type="text" class="form-control" id="txt_username" name="txt_username" placeholder="Digite el usuario" autocomplete="off" required>
					</div>
				</div>

				<!-- COntraseña del nuevo usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_password"><b>Contraseña</b></label>
					<div class="col-sm-9">
						<input maxlength="15" type="password" class="form-control" id="txt_password" name="txt_password" placeholder="Digite la contraseña" autocomplete="off" required>
						<input maxlength="15" type="password" class="form-control" id="txt_password2" name="txt_password2" placeholder="Digite nuevamente la contraseña" autocomplete="off" required>
					</div>
				</div>

				<!-- Check para activar nuevo usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="cbx_password"><b>Activo</b></label>
					<div class="col-sm-9">
						<input type="checkbox" class="form-control" id="cbx_activo" name="cbx_activo" checked="true" value="true">
					</div>
				</div>

				<!-- Opciones de Registrar o Cancelar accion-->
				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_usuarios'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-plus"></i> Registrar</button>
				</div>
			</form>
		</div>

			<!-- Permite actualizar datos de un usuario-->
		<div id="editar_usuario" class="col-sm-6 offset-sm-3">
			<h3 class="text-center">Actualizar Usuario</h3>
			<hr>
			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_EDITAR_USUARIO" onsubmit="editarUsuario(this.id, event);">
				<input type="hidden" value="editarUsuario" name="id_formulario">
				<input type="hidden" name="id_usuario" id="id_usuario3">
				<div class="col-sm-12 form-group row">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="inputGroupSelect01"><b>Rol de Usuario</b></label>
						</div>
						<select class="custom-select" name="slte_rol" id="slte_rol" required="true">
							<?php $roles = Controlador::darRoles();
							for ($i = 0; $i < count($roles); $i++) {	?>
							<option value="<?php echo $roles[$i]->id; ?>"><?php echo $roles[$i]->nombre; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<!-- Nombre nuevo del usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txte_nombres"><b>Nombres</b></label>
					<div class="col-sm-9">
						<input maxlength="100" type="text" class="form-control" id="txte_nombres" name="txte_nombres" placeholder="Digite los nombres" autocomplete="off" required>
					</div>
				</div>

				<!-- Apellidos nuevos del usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txte_apellidos"><b>Apellidos</b></label>
					<div class="col-sm-9">
						<input maxlength="100" type="text" class="form-control" id="txte_apellidos" name="txte_apellidos" placeholder="Digite los apellidos" autocomplete="off" required>
					</div>
				</div>

				<!-- Username del usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txte_username"><b>Username</b></label>
					<div class="col-sm-9">
						<input disabled="true" maxlength="20" type="text" class="form-control" id="txte_username" name="txte_username" placeholder="Digite el usuario" autocomplete="off" required>
					</div>
				</div>

				<!-- COntraseña nueva del usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txte_password"><b>Contraseña</b></label>
					<div class="col-sm-9">
						<input maxlength="15" type="password" class="form-control" id="txte_password" name="txte_password" placeholder="Digite la contraseña" autocomplete="off">
						<input maxlength="15" type="password" class="form-control" id="txte_password2" name="txte_password2" placeholder="Digite nuevamente la contraseña" autocomplete="off">
					</div>
				</div>

				<!-- Check estado activo del usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="cbxe_activo"><b>Activo</b></label>
					<div class="col-sm-9">
						<input type="checkbox" class="form-control" id="cbxe_activo" name="cbxe_activo" checked="true" value="true">
					</div>
				</div>

				<!-- Opciones de Registrar o Cancelar accion-->
				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_usuarios'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-sync-alt"></i> Actualizar</button>
				</div>
			</form>
		</div>

	</div>


	<!-- Opcion para inactivar usuarios registrados-->
	<form action="servidor/controlador.php" method="post" id="FORM_ACTIVAR_INACTIVAR_USUARIO">
		<input type="hidden" value="activarInactivarUsuario" name="id_formulario">
		<input type="hidden" name="id_usuario" id="id_usuario">
		<input type="hidden" name="valor" id="valor">
	</form>

	<!-- Carga lista usuarios registrados-->
	<form action="servidor/controlador.php" method="post" id="FORM_CARGAR_LISTADO_USUARIOS">
		<input type="hidden" value="cargarListadoUsuarios" name="id_formulario">
	</form>

	<!-- Buscar usuario-->
	<form action="servidor/controlador.php" method="post" id="FORM_BUSCAR_USUARIO">
		<input type="hidden" value="buscarUsuario" name="id_formulario">
		<input type="hidden" name="id_usuario" id="id_usuario2">
	</form>


	<script type="text/javascript">
		
		$( document ).ready(function() {
			habilitarPanel('listado_usuarios');
			cargarListadoUsuarios();
		});

		// Habilita panel de Opciones
		function habilitarPanel(id_panel){
			$('#listado_usuarios').hide();
			$('#registrar_usuario').hide();
			$('#editar_usuario').hide();

			$('#'+ id_panel).show();
		}

		// Permite editar registro de un usuario registrado
		function editarUsuario(id_form, event){
			var options = {
				dataType: 'json',
				beforeSubmit: function () {
					spinnerShow();
				},
				success: function(data){
					spinnerHidden();
					if (data.status === 0) {
						alertDanger(false,  data.msg , null);
					} else {
						alertSucess(false, data.msg);
						habilitarPanel('listado_usuarios');
						resetForm(id_form);
						cargarListadoUsuarios();
					}
				}
			};


			// Validar contraseña
			if (validateFormById(id_form)) {
				pass1 = $('#'+id_form +' #txte_password').val();
				pass2 = $('#'+id_form +' #txte_password2').val();
				if(pass1 !== pass2){
					alertWarning(false,'<strong>Las contraseñas no coinciden</strong>');
					$('#'+id_form +' #txte_password').val('');
					$('#'+id_form +' #txte_password2').val('');
				}else{
					$('#' + id_form).ajaxSubmit(options);
				}
			}
			event.preventDefault();
			return false;
		}

		// Activa panel para editar campos de un usuario registrado
		function editarCargarUsuario(id_usuario){
			id_form = "FORM_BUSCAR_USUARIO";
			var options = {
				dataType: 'json',
				beforeSubmit: function () {
					spinnerShow();
				},
				success: function(data){
					spinnerHidden();
					if (data.status === 0) {
						alertDanger(false,  data.msg , null);
					} else {
						habilitarPanel('editar_usuario');
						id_form = "#FORM_EDITAR_USUARIO";
						$(id_form + ' #id_usuario3').val(data.id);
						$(id_form + ' #slte_rol').attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
						$(id_form + ' #slte_rol').find("option[value='"+data.id_rol+"']").attr("selected",true);
						$(id_form + ' #txte_nombres').val(data.nombres);
						$(id_form + ' #txte_apellidos').val(data.apellidos);
						$(id_form + ' #txte_username').val(data.username);
						$(id_form + ' #cbxe_activo' ).prop( 'checked', (data.activo ? true : false) );
					}
				}
			};
			$('#' + id_form + ' #id_usuario2').val(id_usuario);
			$('#' + id_form).ajaxSubmit(options);
		}


		// Permite registrar usuario con exito
		function registrarUsuario(id_form, event){
			var options = {
				dataType: 'json',
				beforeSubmit: function () {
					spinnerShow();
				},
				success: function(data){
					spinnerHidden();
					if (data.status === 0) {
						alertDanger(false,  data.msg , null);
					} else {
						alertSucess(false, data.msg);
						habilitarPanel('listado_usuarios');
						resetForm(id_form);
						cargarListadoUsuarios();
					}
					
				}
			};


			if (validateFormById(id_form)) {
				pass1 = $('#'+id_form +' #txt_password').val();
				pass2 = $('#'+id_form +' #txt_password2').val();
				if(pass1 !== pass2){
					alertWarning(false,'<strong>Las contraseñas no coinciden</strong>');
					$('#'+id_form +' #txt_password').val('');
					$('#'+id_form +' #txt_password2').val('');
				}else{
					$('#' + id_form).ajaxSubmit(options);
				}
				
			}
			event.preventDefault();
			return false;
		}

		// Permite activar o desactivar a un usuario
		function activarInactivar(id_usuario, valor) {
			id_form = 'FORM_ACTIVAR_INACTIVAR_USUARIO';
			var options = {
				dataType: 'json',
				beforeSubmit: function () {
					spinnerShow();
				},
				success: function(data){
					spinnerHidden();
					if (data.status === 0) {
						alertDanger(false,  data.msg , null);
					} else {
						alertSucess(false, data.msg);
						resetForm('FORM_ACTIVAR_INACTIVAR_USUARIO');
						cargarListadoUsuarios();
					}
				}
			};
			$('#' + id_form + ' #id_usuario').val(id_usuario);
			$('#' + id_form + ' #valor').val(valor);
			$('#' + id_form).ajaxSubmit(options);
			return false;
		}


		// Carga lista de usuarios registrados
		function cargarListadoUsuarios(){
			id_form = 'FORM_CARGAR_LISTADO_USUARIOS';
			var options = {
				dataType: 'json',
				beforeSubmit: function () {
					spinnerShow();
				},
				success: cargarListadoDeUsuariosRespuesta
			};
			$('#' + id_form).ajaxSubmit(options);
			return false;
		}

		// Carga datos de los usuarios registrados
		function cargarListadoDeUsuariosRespuesta(data){
			if (data.status === 0) {
				alertDanger(false,  data.msg);
			} else {
				list = $("#TBODY_LISTADO_USUARIOS");
				list.empty();
				for (i = 0; i < data.usuarios.length; i++) {
					user = data.usuarios[i];
					child = '<tr>'
					+ '<th scope="row">' + user.nombres + '</th>'
					+ '<td>' + user.apellidos + '</td>'
					+ '<td>' + user.username + '</td>'
					+ '<td>' + user.rol + '</td>'

					+ '<td>' 
					+ '<button class="btn btn-warning" onclick="editarCargarUsuario(' + user.id + ')" title="Editar"><i class="far fa-edit"></i></button> '
					+ (user.activo == 1  ? 
						'<button class="btn btn-success" onclick="activarInactivar(' + user.id + ' , 0)" title="Inactivar"><i class="fas fa-check-circle"></i></button>' :
						'<button class="btn btn-danger" onclick="activarInactivar(' + user.id + ', 1)" title="Activar"><i class="fas fa-minus-circle"></i></button>')
					+ '</td>'
					+ '</tr>';
					list.append(child);
				}
			}
			spinnerHidden();
		}

	</script>

	</html>