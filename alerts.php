<!-- Configuracion de las alertas de advertencia-->
<div class="alertWarning alert alert-warning alert-dismissable fade show" id="alertWarning" hidden="true" role="alert">
    <a href="#" class="close" aria-label="close" onclick="event.preventDefault();
            $('#alertWarning').attr('hidden', true);">&times;</a>
    <p id="msg"></p>
</div>

<!-- Configuracion de las alertas de exito-->
<div class="alertSuccess alert alert-success alert-dismissable fade show" id="alertSuccess" hidden="true" role="alert">
    <a href="#" class="close" aria-label="close" onclick="event.preventDefault();
            $('#alertSuccess').attr('hidden', true);">&times;</a>
    <p id="msg"></p>
  </button>
</div>

<!-- Configuracion de las alertas de peligro-->
<div class="alertDanger alert alert-danger alert-dismissable fade show" id="alertDanger" hidden="true" role="alert">
    <a href="#" class="close" aria-label="close" onclick="event.preventDefault();
            $('#alertDanger').attr('hidden', true);">&times;</a>
    <p id="msg"></p>
</div>





<div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tituloDialogo">Información</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>