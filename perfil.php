<?php
require_once dirname(__FILE__) . '/servidor/controlador.php';
if (!Controlador::usuarioEstaLogueado()) {
	header("Location: ./");
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<title>Perfil Usuario</title>
	<?php include 'head.php'; ?>

</head>

<body id="page-top">

	<?php include 'nav.php'; ?>

	<section></section> 
	
	<div class="container min-alto">


		<!-- Panel para cambiar la contraseña-->
		<?php include 'alerts.php'; ?>


		<div id="div_inicio" class="col-sm-6 offset-sm-3">
			<h3 class="text-left">Perfil Usuario</h3>
			<hr>

			<div class="col-sm-12">
				<a href="#" class="text-info" onclick="habilitarPanel('div_cambiar_contrasena')">Cambiar Contraseña</a>
			</div>
		</div>


		<!-- Opcion para registrar un nuevo usuario-->
		<div id="div_cambiar_contrasena" class="col-sm-6 offset-sm-3">
			<h3 class="text-center">Cambiar Contraseña</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_CAMBIAR_CONTRASEÑA" onsubmit="cambiarContrasena(this.id, event);">
				<input type="hidden" value="cambiarContrasena" name="id_formulario">
				
				<!--Nueva COntraseña-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_password"><b>Nueva Contraseña</b></label>
					<div class="col-sm-9">
						<input maxlength="15" type="password" class="form-control" id="txt_password" name="txt_password" placeholder="Digite la contraseña" autocomplete="off" required>
						<input maxlength="15" type="password" class="form-control" id="txt_password2" name="txt_password2" placeholder="Digite nuevamente la contraseña" autocomplete="off" required>
					</div>
				</div>

				<!-- Opciones de Registrar o Cancelar accion-->
				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('div_inicio'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-plus"></i> Registrar</button>
				</div>
			</form>
		</div>

	</div>


	<script type="text/javascript">
		
		$( document ).ready(function() {
			habilitarPanel('div_inicio');
		});

		// Habilita panel de Opciones
		function habilitarPanel(id_panel){
			$('#div_inicio').hide();
			$('#div_cambiar_contrasena').hide();

			$('#'+ id_panel).show();
		}

		// Permite cambiar la contraseña
		function cambiarContrasena(id_form, event){
			var options = {
				dataType: 'json',
				beforeSubmit: function () {
					spinnerShow();
				},
				success: function(data){
					spinnerHidden();
					if (data.status === 0) {
						alertDanger(false, data.msg , null);
					} else {
						alertSucess(false, data.msg);
						habilitarPanel('div_inicio');
						resetForm(id_form);
					}
					
				}
			};


			if (validateFormById(id_form)) {
				pass1 = $('#'+id_form +' #txt_password').val();
				pass2 = $('#'+id_form +' #txt_password2').val();
				if(pass1 !== pass2){
					alertWarning(false,'Las contraseñas no coinciden');
					$('#'+id_form +' #txt_password').val('');
					$('#'+id_form +' #txt_password2').val('');
				}else{
					$('#' + id_form).ajaxSubmit(options);
				}
				
			}
			event.preventDefault();
			return false;
		}

	</script>

	</html>