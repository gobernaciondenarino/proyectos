<?php
require_once dirname(__FILE__) . '/servidor/controlador.php';
if (!Controlador::usuarioEstaLogueado()) {
	header("Location: ./");
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<title>Ingresar</title>
	<?php include 'head.php'; ?>
	
	<link rel="stylesheet" type="text/css" href="recursos/js/datetimepicker-master/datetimepicker-master/jquery.datetimepicker.css"/ >
	<script src="recursos/js/datetimepicker-master/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>

	<script>
	$( function() {
		$( "#txte_fecha_respuesta_uv" ).datetimepicker({
			format:'Y-m-d H:i',
			lang:'es'
		});
	} );

	</script>

</head>

<body id="page-top">

	<?php include 'nav.php'; ?>
	<section></section>
	<div class="container min-alto">

		<?php include 'alerts.php'; ?>

		<!-- Carga la lista de los proyectos registrados-->
		<div id="listado_proyectos" class="col-sm-12">
			<div class="row form-group">
				<h3 class="text-left col-sm-11">Proyectos</h3>
			</div>

			<div class="col-sm-12 ">
				<form id="form_busqueda" class="form-horizontal">
					<div class="form-group input-group mb-3">
						
						<div class="input-group-prepend" id="button-addon3">
							<button  type="button" class="btn btn-success btn-lg" onclick="habilitarPanel('registrar_proyecto')" title="Nuevo Proyecto"><i class="fas fa-plus"></i></button>
						</div>
						<input type="text" class="form-control" id="txtSearch" name="txtSearch" placeholder="Buscar por (identificador ,nombre , creador, modelo o estado)" required="true" autocomplete="off" onkeyup="buscarProyectos(this.form , event);">
						<div class="input-group-append" id="button-addon4">

							<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i> Buscar</span>

							<button type="button" class="btn btn-info btn-lg" onclick="limpiarForm(this.form);" title="Limpiar"><i class="fas fa-broom"></i></button>

						</div>

					</div>

					<!-- Busqueda de proyectos por modelo, estado, usuario-->
					<div class="row">
						<div class="col-sm-4">
							<select name="slt_mc" id="slt_mc" class="custom-select custom-select-lg mb-3" onchange="buscarProyectos(this.form , event);">
								<option value="-1">-- Modelo de Contratación --</option>
								<?php $modelos = Controlador::darModelosContratacion();
								for ($i = 0; $i < count($modelos); $i++) {	?>
								<option value="<?php echo $modelos[$i]->id; ?>"><?php echo $modelos[$i]->modelo; ?></option>
								<?php } ?>
							</select>	
						</div>

						<div class="col-sm-4">
							<select name="slt_e" id="slt_e" class="custom-select custom-select-lg mb-3" onchange="buscarProyectos(this.form , event);">
								<option selected value="-1">-- Estado de Proyecto --</option>
								<?php $estados = Controlador::darEstados();
								for ($i = 0; $i < count($estados); $i++) {	?>
								<option value="<?php echo $estados[$i]->id; ?>"><?php echo $estados[$i]->estado; ?></option>
								<?php } ?>
							</select>		
						</div>

						<div class="col-sm-4">
							<select name="slt_u" id="slt_u" class="custom-select custom-select-lg mb-3" onchange="buscarProyectos(this.form , event);">
								<option selected value="-1">-- Creado Por --</option>
								<?php $usuarios = Controlador::darUsuarios();
								for ($i = 0; $i < count($usuarios); $i++) {	?>
								<option value="<?php echo $usuarios[$i]->id; ?>"><?php echo $usuarios[$i]->nombres . ' ' . $usuarios[$i]->apellidos ; ?></option>
								<?php } ?>
							</select>		
						</div>
					</div>
				</form>
			</div>

			<!-- Encabezados de tabla para mostrar proyectos-->
			<div class="col-sm-12" style="overflow: auto; max-height: 400px; width: 100%;">
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">ID</th>
							<th>Proyecto</th>
							<th>Creador</th>
							<th>Modelo Contratación</th>
							<th>Estado Proyecto</th>
							<th>Fecha Estado</th>
							<th>Ultima Modificación</th>
							<th width="180">Opciones</th>
						</tr>
					</thead>
					<tbody id="TBODY_LISTADO_PROYECTOS" >
						
					</tbody>
				</table>
			</div>
		</div>

		<!-- Permite crear un nuevo proyecto-->
		<div id="registrar_proyecto" class="col-sm-9 offset-sm-1">
			<h3 class="text-center">Crear Proyecto</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_REGISTRAR_PROYECTO" onsubmit="registrarProyecto(this.id, event);">
				<input type="hidden" value="registrarProyecto" name="id_formulario">

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_nombre"><b>Nombre Proyecto</b></label>
					<div class="col-sm-9">
						<input maxlength="100" type="text" class="form-control" id="txt_nombre" name="txt_nombre" placeholder="Digite el nombre del proyecto" autocomplete="off" required>
					</div>
				</div>

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_creado_por"><b>Creado Por</b></label>
					<div class="col-sm-9">
						<input value="<?php echo Controlador::darNombreCompletoUsuario();?>" disabled="true" maxlength="100" type="text" class="form-control" id="txt_creado_por" name="txt_creado_por" placeholder="Digite los apellidos" autocomplete="off" required>
					</div>
				</div>

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="slt_modelo_contratacion"><b>Modelo Contratación</b></label>
					<div class="col-sm-9">
						<select class="custom-select" name="slt_modelo_contratacion" id="slt_modelo_contratacion" required="true">
							<?php $modelos = Controlador::darModelosContratacion();
							for ($i = 0; $i < count($modelos); $i++) {	?>
							<option value="<?php echo $modelos[$i]->id; ?>"><?php echo $modelos[$i]->modelo; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="slt_estado"><b>Estado</b></label>
					<div class="col-sm-9">
						<select class="custom-select" name="slt_estado" id="slt_estado" required="true">
							<?php $estados = Controlador::darEstados();
							$encontro = false;
							for ($i = 0; $i < count($estados); $i++) {	
								if($estados[$i]->estado_inicial == 1){
									$encontro = true;?>
									<option selected value="<?php echo $estados[$i]->id; ?>"><?php echo $estados[$i]->estado; ?></option>
									<?php
									break;
								}
							}
							if($encontro == false){
								echo '<option value="" selected disabled hidden >No hay un estado inicial para el proyecto</option>';
							} ?>

						</select>
					</div>
				</div>


				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="inputFile"><b>Cargar documento</b></label>
					<div class="col-sm-9" id="inputs">
						<input multiple type="file" id="inputFile" name="inputFile[]"  onchange="validate(this)" onclick="loadTypesPerm(this);" required >
						<div class="help-block with-errors"></div>
						<p class="help-block" id="msgMaxFiles"><script>getMsgMaxFile('msgMaxFiles');</script></p>    
					</div>
				</div>

				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_proyectos'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-plus"></i> Crear</button>
				</div>
			</form>
		</div>



		<div id="editar_proyecto" class="col-sm-12">


			<h3 class="text-center">Actualizar Proyecto</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_EDITAR_PROYECTO" onsubmit="actualizarProyecto(this.id, event);">
				<input type="hidden" value="actualizarProyecto" name="id_formulario">
				<input type="hidden" name="id_proyecto" id="id_proyecto2">

				<div class="col-sm-12 form-group row">

					<div class="col-sm-6">
						<label class="col-sm-12 control-label" for="txte_creado_por"><b>Creado Por</b></label>
						<div class="col-sm-12">
							<input disabled="true" maxlength="100" type="text" class="form-control" id="txte_creado_por" name="txte_creado_por" placeholder="Digite los apellidos">
						</div>
					</div>

					<div class="col-sm-6">
						<label class="col-sm-12 control-label" for="txte_nombre"><b>Nombre Proyecto</b></label>
						<div class="col-sm-12">
							<input maxlength="100" type="text" class="form-control" id="txte_nombre" name="txte_nombre" placeholder="Digite el nombre del proyecto" autocomplete="off" required>
						</div>
					</div>

				</div>



				<div class="col-sm-12 form-group row">
					<div class="col-sm-4">
						<label class="col-sm-12 control-label" for="txte_fecha_radicado"><b>Fecha Estado</b></label>
						<div class="col-sm-12">
							<input disabled="true" type="text" class="form-control" id="txte_fecha_radicado" name="txte_fecha_radicado"  autocomplete="off">
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-sm-12 control-label" for="txte_fecha_ultima_modificacion"><b>Ultima Modificación</b></label>
						<div class="col-sm-12">
							<input disabled="true" type="text" class="form-control" id="txte_fecha_ultima_modificacion" name="txte_fecha_ultima_modificacion"  autocomplete="off">
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-sm-12 control-label" for="txte_ultima_version"><b>Ultima Versión</b></label>
						<div class="col-sm-12">
							<input disabled="true" type="text" class="form-control" id="txte_ultima_version" name="txte_ultima_version"  autocomplete="off">
						</div>
					</div>

				</div>

				<div class="col-sm-12 form-group row">



				</div>

				<div class="col-sm-12 form-group row mt-1">
					<div class="col-sm-6  form-group">
						<div class="input-group mb-3">
							<label class="col-sm-12 control-label" for="slte_modelo_contratacion"><b>Modelo Contratacicón</b></label>
							<div class="input-group-prepend">
								<label class="input-group-text" for="slte_modelo_contratacion"><b>Modelo Contratación</b></label>
							</div>
							<select class="custom-select" name="slte_modelo_contratacion" id="slte_modelo_contratacion" required="true">
								<?php $modelos = Controlador::darModelosContratacion();
								for ($i = 0; $i < count($modelos); $i++) {	?>
								<option value="<?php echo $modelos[$i]->id; ?>"><?php echo $modelos[$i]->modelo; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="col-sm-6 form-group">
						<div class="input-group mb-3">
							<label class="col-sm-12 control-label" for="slte_estado"><b>Estado</b></label>
							<div class="input-group-prepend">
								<label class="input-group-text" for="slte_estado"><b>Estado</b></label>
							</div>
							<select class="custom-select" name="slte_estado" id="slte_estado" required="true">
								<?php $estados = Controlador::darEstados();
								for ($i = 0; $i < count($estados); $i++) {	?>
								<option value="<?php echo $estados[$i]->id; ?>"><?php echo $estados[$i]->estado; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>


				<div class="col-sm-12 form-group row mt-1">
					<div class="col-sm-6 form-group row">
						<label class="col-sm-12 control-label" for="inputFile">Cargar Nueva Versión</label>
						<div class="col-sm-12" id="inputs">
							<input multiple type="file" id="inputFile" name="inputFile[]"  onchange="validate(this)" onclick="loadTypesPerm(this);" >
							<div class="help-block with-errors"></div>
							<p class="help-block" id="msgMaxFiles"><script>getMsgMaxFile('msgMaxFiles');</script></p>    
						</div>
					</div>

					<div class="col-sm-6 text-right">
						<button type="submit" class="btn btn-info btn-lg" title="Actualizar Proyecto"><i class="fas fa-sync-alt"></i> Actualizar</button>
					</div>

				</div>
			</form>

			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_ACTUALIZAR_VERSION" onsubmit="actualizarVersion(this.id, event);">
				<input type="hidden" value="actualizarVersion" name="id_formulario">
				<input type="hidden" name="id_version" id="id_version">

				<?php include 'alerts.php'; ?>

				<div class="col-sm-12 form-group row">
					<div class="offset-md-3 col-md-6 col-sm-12 form-group row">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<label class="input-group-text" for="slte_version"><b>Versiones</b></label>
							</div>
							<select class="custom-select" name="slte_version" id="slte_version" required="true" onchange="buscarVersion(this.value);">

							</select>
						</div>
					</div>

					<div class="offset-md-3 col-md-6 col-sm-12">
						<ul class="list-group" id="lista_archivos">

						<ul >
					</div>
				</div>

				<div class="col-sm-12 form-group row">
					<div class="col-sm-6 row">
						<label class="col-sm-12 control-label" for="txte_fecha_enviado_uv"><b>Fecha Enviado</b></label>
						<div class="col-sm-12">
							<input disabled="true" type="text" class="form-control" id="txte_fecha_enviado_uv" name="txte_fecha_enviado_uv"  autocomplete="off">
						</div>
					</div>

					<div class="col-sm-6 row">
						<label class="col-sm-12 control-label" for="txte_fecha_respuesta_uv"><b>Fecha Respuesta</b></label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txte_fecha_respuesta_uv" name="txte_fecha_respuesta_uv"  autocomplete="off">
						</div>
					</div>
				</div>


				<div class="col-sm-12 form-group row">
					<div class="col-sm-6 row">
						<label class="col-sm-12 control-label" for="txte_obs_tec"><b>Observación Tecnica</b></label>
						<div class="col-sm-12">
							<textarea  rows="10" class="form-control" id="txte_obs_tec" name="txte_obs_tec" maxlength="500"></textarea>
							<!--<?php //echo ( Controlador::darNivelPermiso() == Controlador::$NIVEL_ADMINISTRADOR) ? '': 'readonly'; ?>-->
						</div>
					</div>

					<div class="col-sm-6 row">
						<label class="col-sm-12 control-label" for="txte_obs_jur"><b>Observación Juridica</b></label>
						<div class="col-sm-12">
							<textarea  rows="10" class="form-control" id="txte_obs_jur" name="txte_obs_jur" maxlength="500"></textarea>
							<!--<?php //echo ( Controlador::darNivelPermiso() == Controlador::$NIVEL_ADMINISTRADOR) ? 'readonly': ''; ?>-->
						</div>
					</div>
				</div>


				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_proyectos'); cargarListadoProyectos($('#txtSearch').val()); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-plus"></i> Guardar</button>
				</div>
			</form>
		</div>


		<div id="visualizar_proyecto" class="col-sm-12">
			<h3 class="text-center">Ver Proyecto</h3>
			<hr>

			<div class="col-sm-12 jumbotron">
				<h1 class="display-4" id="nombre_proyecto"></h1>
				<hr class="my-4">



<!-- Panel de visualizacion de un proyecto-->
<div class="col-sm-12" style="overflow: auto; max-height: 400px; width: 100%;">
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th >ID</th>
							<th>Creado Por</th>
							<th>Fecha Radicación</th>
							<th>Fecha Última Modificación</th>
						</tr>
					</thead>
					<tbody id="TBODY_LISTADO_PROYECTOS" >
						<tr>
							<td id="id"></td>
							<td id="creado_por"></td>
							<td id="fecha_radicacion"></td>
							<td id="fecha_ultima_modificacion"></td>
						</tr>
					</tbody>
				</table>
			</div>



				

				<!-- Panel de visualizacion de un proyecto-->
				<!--<h1 id="version"></h1>-->
			</div>
			<div class="col-sm-12 form-group">	
				<ul class="list-group" id="lista_versiones">
					<li class="list-group-item active">Listado de Versiones</li>
					<li class="list-group-item">
						<div class="col-sm-12" style="overflow: auto; max-height: 400px; width: 100%;">
							<table class="table">
								<thead class="thead-dark">
									<tr>
										<th scope="col"># Versión</th>
										<th>Modificado Por</th>
										<th>Fecha Envío</th>
										<th>Fecha Respuesta</th>
										<th>Documento</th>
									</tr>
								</thead>
								<tbody id="TBODY_LISTADO_VERSIONES" >

								</tbody>
							</table>
						</div>
					</li>
				</ul>
			</div>

			<div class="col-sm-12 text-right">
				<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_proyectos');"><i class="fas fa-arrow-left"></i> Volver</button>
			</div>
		</div>

	</div>

	<form action="servidor/controlador.php" method="post" id="FORM_CARGAR_LISTADO_PROYECTOS">
		<input type="hidden" value="cargarListadoProyectos" name="id_formulario">
		<input type="hidden" value="" id="txt_busqueda" name="txt_busqueda">
		<input type="hidden" value="" id="slt_modelo_c" name="slt_modelo_c">
		<input type="hidden" value="" id="slt_estado" name="slt_estado">
		<input type="hidden" value="" id="slt_usuario" name="slt_usuario">
	</form>

	<form action="servidor/controlador.php" method="post" id="FORM_BUSCAR_PROYECTOS">
		<input type="hidden" value="buscarProyecto" name="id_formulario">
		<input type="hidden" id="id_proyecto" name="id_proyecto">
	</form>


	<form action="servidor/controlador.php" method="post" id="FORM_BUSCAR_VERSION">
		<input type="hidden" value="buscarVersion" name="id_formulario">
		<input type="hidden" id="id_version" name="id_version">
	</form>

	<form action="servidor/controlador.php" method="post" id="FORM_ACTIVAR_INACTIVAR_PROYECTO">
		<input type="hidden" value="activarInactivarProyecto" name="id_formulario">
		<input type="hidden" name="id_proyecto" id="id_proyecto">
		<input type="hidden" name="valor" id="valor">
	</form>

</body>

<script type="text/javascript">

$( document ).ready(function() {
	habilitarPanel('listado_proyectos');
	cargarListadoProyectos('');
});

// Habilita panel de proyectos
function habilitarPanel(id_panel){
	$('#listado_proyectos').hide();
	$('#registrar_proyecto').hide();
	$('#editar_proyecto').hide();
	$('#visualizar_proyecto').hide();

	$('#'+ id_panel).show();
}

// Buscar version del proyecto
function buscarVersion(id_version){
	id_form = 'FORM_BUSCAR_VERSION';
	$('#'+id_form +' #id_version').val(id_version);
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false, data.msg , null);
			} else {
				id_form = "#FORM_ACTUALIZAR_VERSION";
				$(id_form + ' #id_version').val(data.id_version);
				$(id_form + ' #txte_fecha_enviado_uv').val(data.fecha_envio_uv);
				$(id_form + ' #txte_fecha_respuesta_uv').val(data.fecha_respuesta_uv);
				$(id_form + ' #txte_obs_jur').val(data.obs_juridica);
				$(id_form + ' #txte_obs_tec').val(data.obs_tecnica);
				list = $(id_form + ' #lista_archivos');
				list.empty();
				for (i = 0; i < data.archivos.length; i++) {
					archivo = data.archivos[i];
					child = '<li class="list-group-item">'
					+ '<a style="width:100%;" id="btn_descargar_version" class="btn btn-dark btn-sm" target="_blank" href="' + archivo.ruta_descarga + '"><i class="fas fa-download"></i> ' + archivo.nombre_archivo + '</a></small>'
					+ '</li>';
					list.append(child);
				}		
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	$('#' + id_form).ajaxSubmit(options);				
	return false;
}

// Actualiza la version de un proyecto
function actualizarVersion(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false, data.msg , null);
			} else {
				alertSucess(false, data.msg);
				id_proyecto = $('#FORM_EDITAR_PROYECTO #id_proyecto2').val();
				//editarProyecto(id_proyecto);
				habilitarPanel('listado_proyectos'); 
				cargarListadoProyectos($('#txtSearch').val()); 
				resetForm(id_form);
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}


// Actualiza campos de un proyecto
function actualizarProyecto(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false, data.msg , null);
			} else {
				alertSucess(false, data.msg);
				id_proyecto = $('#'+id_form +' #id_proyecto2').val();
				editarProyecto(id_proyecto);
				//habilitarPanel('listado_proyectos'); 
				cargarListadoProyectos($('#txtSearch').val()); 
				resetForm(id_form);
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}

// Permite editar un proyecto
function editarProyecto(id_proyecto){
	habilitarPanel('editar_proyecto');
	id_form = 'FORM_BUSCAR_PROYECTOS';
	$('#'+id_form +' #id_proyecto').val(id_proyecto);

	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false, data.msg , null);
			} else {
				habilitarPanel('editar_proyecto');
				id_form = "#FORM_EDITAR_PROYECTO";
				$(id_form + ' #id_proyecto2').val(data.id_proyecto);
				$(id_form + ' #txte_creado_por').val(data.creado_por);
				$(id_form + ' #txte_nombre').val(data.nombre);
				$(id_form + ' #slte_modelo_contratacion').attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
				$(id_form + ' #slte_modelo_contratacion').find("option[value='"+data.id_modelo+"']").attr("selected",true);
				$(id_form + ' #slte_estado').attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
				$(id_form + ' #slte_estado').find("option[value='"+data.id_estado+"']").attr("selected",true);
				$(id_form + ' #txte_fecha_radicado').val(data.fecha_radicacion);
				$(id_form + ' #txte_fecha_ultima_modificacion').val(data.fecha_ultima_modificacion);
				$(id_form + ' #txte_ultima_version').val(data.ultima_version);
				$(id_form + ' #slte_version').attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
				if(data.versiones && data.versiones.length > 0){

					id_form = "#FORM_ACTUALIZAR_VERSION";
					$(id_form + ' #id_version').val(data.id_version);
					slc = $(id_form + ' #slte_version');
					slc.empty();
					options = '';
					for (i = 0; i < data.versiones.length; i++) {
						options += '<option value="'+data.versiones[i].id_version+'">'+data.versiones[i].numero+'</option>';
					}
					slc.append(options);
					slc.find("option[value='"+data.id_version+"']").attr("selected",true);

					$(id_form + ' #txte_fecha_enviado_uv').val(data.fecha_envio_uv);
					$(id_form + ' #txte_fecha_respuesta_uv').val(data.fecha_respuesta_uv);
					$(id_form + ' #txte_obs_jur').val(data.obs_juridica);
					$(id_form + ' #txte_obs_tec').val(data.obs_tecnica);
					
					list = $(id_form + ' #lista_archivos');
					list.empty();
					for (i = 0; i < data.archivos.length; i++) {
						archivo = data.archivos[i];
						child = '<li class="list-group-item">'
						+ '<a style="width:100%;" id="btn_descargar_version" class="btn btn-dark btn-sm" target="_blank" href="' + archivo.ruta_descarga + '"><i class="fas fa-download"></i> ' + archivo.nombre_archivo + '</a></small>'
						+ '</li>';
						list.append(child);
					}					
				}

			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}

// Permite visualizar un proyecto
function visualizarProyecto(id_proyecto){
	habilitarPanel('visualizar_proyecto');
	id_form = 'FORM_BUSCAR_PROYECTOS';
	$('#'+id_form +' #id_proyecto').val(id_proyecto);
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false,  data.msg , null);
			} else {
				id_div = 'visualizar_proyecto';
				$('#'+id_div + ' #nombre_proyecto').text(data.nombre);
				$('#'+id_div + ' #id').html(data.id_proyecto);
				$('#'+id_div + ' #creado_por').html(data.creado_por);
				$('#'+id_div + ' #fecha_radicacion').html(data.fecha_radicacion);
				$('#'+id_div + ' #fecha_ultima_modificacion').html(data.fecha_ultima_modificacion);
				//$('#'+id_div + ' #version').html('<span class="badge badge-secondary">Version '+data.ultima_version+'</span>');
				
				/*lista ='<li class="list-group-item active">Listado de Versiones</li>';

				for (var i = 0; i < data.versiones.length; i++) {
					version = data.versiones[i];
					lista += '<li class="list-group-item"><b>#'+version.numero+'</b><br/>'
					+'<b>Fecha Enviado:</b> '+version.fecha_envio+'<br/>'
					+'<b>Fecha Respuesta:</b> '+version.fecha_respuesta+'<br/>'
					+'<a href="'+version.ruta_descarga+'" class="btn btn-dark btn-sm" target="_blank"><i class="fas fa-download"></i>'+version.nombre_archivo+'</a></li>';
				}
				$('#'+id_div + ' #lista_versiones').html(lista);*/

				list = $('#'+id_div + ' #TBODY_LISTADO_VERSIONES');
				list.empty();
				for (var i = 0; i < data.versiones.length; i++) {
					version = data.versiones[i];
					child = '<tr>'
					+ '<th scope="row">' + version.numero + '</th>'
					+ '<td>' + version.modificado_por + '</td>'
					+ '<td>' + version.fecha_envio + '</td>'
					+ '<td>' + version.fecha_respuesta + '</td>'
					+ '<td><ul>';
					
					for (j = 0; j < version.archivos.length; j++) {
						archivo = version.archivos[j];
						child += '<li>'
						+ '<a title=" '+archivo.nombre_archivo+'" href="'+archivo.ruta_descarga+'" class="btn btn-sm" target="_blank" style="width:200px; text-align: left;"><i class="fas fa-download"></i> '+archivo.nombre_archivo+'</a>'
						+ '</li>';
					}	

					child += '</ul></td></tr>';
					list.append(child);
				}
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	$('#' + id_form).ajaxSubmit(options);		
	return false;
}

// Limpia un fomrulario
function limpiarForm(form){
	form.txtSearch.value = '';
	$('#'+form.id + ' #slt_mc').val('-1');
	$('#'+form.id + ' #slt_e').val('-1');
	//$('#'+form.id + ' #slt_mc').attr('selectedIndex', '0').find("option:selected").removeAttr("selected");
	//$('#'+form.id + ' #slt_e').attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	buscarProyectos(form, null)
}

// Busca un proyecto
function buscarProyectos(form, event){
	cargarListadoProyectos(form.txtSearch.value, form.slt_mc.value, form.slt_e.value,form.slt_u.value);	
	if(event != null){
		event.preventDefault();	
	}
	return false;
}

// Permite registrar un prpoyecto
function registrarProyecto(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false, data.msg , null);
			} else {
				alertSucess(false, data.msg);
				habilitarPanel('listado_proyectos');
				resetForm(id_form);
				cargarListadoProyectos('');
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}

// Carga la lista de todos los proyectos
function cargarListadoProyectos(busqueda , id_modelo, id_estado, id_usuario){
	id_form = 'FORM_CARGAR_LISTADO_PROYECTOS';
	$('#'+id_form +' #txt_busqueda').val(busqueda);
	$('#'+id_form +' #slt_modelo_c').val(id_modelo);
	$('#'+id_form +' #slt_estado').val(id_estado);
	$('#'+id_form +' #slt_usuario').val(id_usuario);
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
				//spinnerShow();
			},
			success: function(data){
				if (data.status === 0) {
					alertDanger(false, data.msg);
				} else {
					list = $("#TBODY_LISTADO_PROYECTOS");
					list.empty();
					for (i = 0; i < data.proyectos.length; i++) {
						p = data.proyectos[i];
						if(data.nivel_permiso == 1 || p.activo == 1){
							child = '<tr>'
							+ '<th scope="row">' + p.id + '</th>'
							+ '<td>' + p.nombre + '</td>'
							+ '<td>' + p.creado_por + '</td>'
							+ '<td>' + p.modelo + '</td>'
							+ '<td>' + p.estado + '</td>'
							+ '<td>' + p.fecha_radicacion + '</td>'
							+ '<td>' + p.fecha_ultima_modificacion + '</td>'

							+ '<td>' 

							+ (data.nivel_permiso == 3 ? 
								'': 
								'<button class="btn btn-warning" onclick="editarProyecto(' + p.id + ')" title="Editar"><i class="far fa-edit"></i></button> ')

							+ (data.nivel_permiso == 1 ? p.activo == 1  ? 
								'<button class="btn btn-success" onclick="activarInactivar(' + p.id + ' , 0)" title="Eliminar"><i class="fas fa-check-circle"></i></button>' :
								'<button class="btn btn-danger" onclick="activarInactivar(' + p.id + ', 1)" title="Recuperar"><i class="fas fa-minus-circle"></i></button>' 
								:'')

							+ (data.nivel_permiso == 2 && p.activo == 1  ? 
								'<button class="btn btn-success" onclick="activarInactivar(' + p.id + ' , 0)" title="Eliminar"><i class="fas fa-check-circle"></i></button>' : '')

							+ '<button style="margin-left: 4px;" class="btn btn-dark" onclick="visualizarProyecto(' + p.id + ', 1)" title="Visualizar"><i class="far fa-eye"></i></button>'
							+ '</td>'
							+ '</tr>';
							list.append(child);
						}
					}
				}
			},
			error: function(data){
				console.log(data.responseText);
			}
		};
		$('#' + id_form).ajaxSubmit(options);
		return false;
	}

	//  Permite activar o inactivar un proyecto
	function activarInactivar(id_proyecto, valor) {
		id_form = 'FORM_ACTIVAR_INACTIVAR_PROYECTO';
		var options = {
			dataType: 'json',
			beforeSubmit: function () {
				spinnerShow();
			},
			success: function(data){
				if (data.status === 0) {
					alertDanger(false, data.msg , null);
				} else {
					alertSucess(false, data.msg);
					resetForm('FORM_ACTIVAR_INACTIVAR_PROYECTO');
					cargarListadoProyectos('');
				}
				spinnerHidden2();
			},
			error: function(data){
				console.log(data.responseText);
			}
		};
		$('#' + id_form + ' #id_proyecto').val(id_proyecto);
		$('#' + id_form + ' #valor').val(valor);
		$('#' + id_form).ajaxSubmit(options);
		return false;
	}
	</script>
	</html>