<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap pra el CSS -->
<link href="recursos/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Fuentes personalizadas para esta plantilla -->
<link href="recursos/vendor/font-awesome/css/all.css" rel="stylesheet" type="text/css">
<link href="recursos/fonts/fonts1.css" rel="stylesheet" type="text/css">
<link href="recursos/fonts/fonts2.css" rel="stylesheet" type="text/css">

<!-- Plugin CSS -->
<link href="recursos/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

<!-- Estilos personalizados para esta plantilla -->
<link href="recursos/css/freelancer.min.css" rel="stylesheet">
<link href="recursos/css/estilos.css" rel="stylesheet">
<link rel="shortcut icon" href="recursos/img/favicon.ico">
<script type="text/javascript" src="recursos/js/jquery.min.js" id="nnn"></script>
<script type="text/javascript" src="recursos/js/jquery.form.min.js"></script>
<script type="text/javascript" src="recursos/js/functions.js"></script>
<script src="recursos/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>