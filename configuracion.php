<?php

// Clase que me permite configurar los ESTADOS y MODELOS del proyecto.

require_once dirname(__FILE__) . '/servidor/controlador.php';
if (!Controlador::usuarioEstaLogueado() || Controlador::darNivelPermiso() != Controlador::$NIVEL_ADMINISTRADOR) {
	header("Location: ./");
}

?>

<!DOCTYPE html>
<html lang="es">

<!-- Cabecera de las opciones de configuraciones de los proyectos-->
<head>
	<title>Configuración</title>
	<?php include 'head.php'; ?>

</head>


<body id="page-top">

	<?php include 'nav.php'; ?>
	<section></section>
	<div class="container min-alto">

		<?php include 'alerts.php'; ?>
   
		<div id="listado_configuraciones" class="col-sm-12">
			<div class="row form-group">
				<h3 class="text-left col-sm-11">Configuración del Sistema</h3>
			</div>

			<hr>

			<div class="col-sm-12 row">	
				<div class="col-md-5 col-sm-12 form-group offset-md-1" style="border-right: 0.5px solid #f1f1f1;">	
					<div class="col-sm-12 row form-group">
						<h5 class="text-left col-sm-10">Estados de proyecto</h5>

						<button  type="button" class="btn btn-success" onclick="habilitarPanel('registrar_estado')" title="Nuevo Estado de proyecto"><i class="fas fa-plus"></i></button>

					</div>

					<!-- Listado de estados NOMBRE y OPCIONES -->
					<div class="col-sm-12" style="overflow: auto; max-height: 400px; width: 100%;">
						<table class="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Nombre</th>
									<th width="20">Opciones</th>
								</tr>
							</thead>
							<tbody id="TBODY_LISTADO_ESTADOS" >

							</tbody>
						</table>
					</div>
				</div> 

    
				<div class="col-md-5 col-sm-12 form-group" style="border-left: 0.5px solid #f1f1f1;">	
					<div class="col-sm-12 row form-group">
						<h5 class="text-left col-sm-10">Modelos de contratación</h5>

						<button  type="button" class="btn btn-success" onclick="habilitarPanel('registrar_modelo')" title="Nuevo Modelo de contratación"><i class="fas fa-plus"></i></button>

					</div>

					<!-- Listado de modelos NOMBRE y OPCIONES -->
					<div class="col-sm-12" style="overflow: auto; max-height: 400px; width: 100%;">
						<table class="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Nombre</th>
									<th width="20">Opciones</th>
								</tr>
							</thead>
							<tbody id="TBODY_LISTADO_MODELOS" >

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<!-- Permite crear ESTADOS -->
		<div id="registrar_estado" class="col-sm-9 offset-sm-1">
			<h3 class="text-center">Crear Estado</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_REGISTRAR_ESTADO" onsubmit="registrarEstado(this.id, event);">
				<input type="hidden" value="registrarEstado" name="id_formulario">

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_estado"><b>Estado</b></label>
					<div class="col-sm-9">
						<input maxlength="50" type="text" class="form-control" id="txt_estado" name="txt_estado" placeholder="Digite el nombre del estado de proyecto" autocomplete="off" required>
					</div>
				</div>

				<!-- Check para activar nuevo usuario-->
				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="cbx_estado_inicial"><b>Estado Inicial</b></label>
					<div class="col-sm-1">
						<input type="checkbox" class="form-control" id="cbx_estado_inicial" name="cbx_estado_inicial" value="false">
					</div>
				</div>

				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_configuraciones'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-plus"></i> Crear</button>
				</div>
			</form>
		</div>


		<!-- Permite editar ESTADOS -->
		<div id="editar_estado" class="col-sm-12">
			<h3 class="text-center">Editar Estado</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_EDITAR_ESTADO" onsubmit="editarEstado(this.id, event);">
				<input type="hidden" value="editarEstado" name="id_formulario">
				<input type="hidden" name="id_estado" id="id_estado">

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_estado"><b>Estado</b></label>
					<div class="col-sm-9">
						<input maxlength="50" type="text" class="form-control" id="txt_estado" name="txt_estado" placeholder="Digite el nombre del estado de" autocomplete="off" required>
					</div>
				</div>

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="cbxe_estado_inicial"><b>Estado Inicial</b></label>
					<div class="col-sm-1">
						<input type="checkbox" class="form-control" id="cbxe_estado_inicial" name="cbxe_estado_inicial" value="false">
					</div>
				</div>

				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_configuraciones'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-info btn-lg" title="Actualizar Estado"><i class="fas fa-sync-alt"></i> Actualizar</button>
				</div>

			</form>
		</div>

		<!-- Permite crear MODELOS -->
		<div id="registrar_modelo" class="col-sm-9 offset-sm-1">
			<h3 class="text-center">Crear Modelo</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_REGISTRAR_MODELO" onsubmit="registrarModelo(this.id, event);">
				<input type="hidden" value="registrarModelo" name="id_formulario">

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_modelo"><b>Modelo</b></label>
					<div class="col-sm-9">
						<input maxlength="50" type="text" class="form-control" id="txt_modelo" name="txt_modelo" placeholder="Digite el nombre del modelo de proyecto" autocomplete="off" required>
					</div>
				</div>

				<hr>
				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_configuraciones'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-success btn-lg"><i class="fas fa-plus"></i> Crear</button>
				</div>
			</form>
		</div>

		<!-- Permite editar MODELOS -->
		<div id="editar_modelo" class="col-sm-12">
			<h3 class="text-center">Editar Modelo</h3>
			<hr>

			<form action="servidor/controlador.php" method="post" class="form-horizontal" id="FORM_EDITAR_MODELO" onsubmit="editarModelo(this.id, event);">
				<input type="hidden" value="editarModelo" name="id_formulario">
				<input type="hidden" name="id_modelo" id="id_modelo">

				<div class="col-sm-12 form-group row">
					<label class="col-sm-3 control-label" for="txt_modelo"><b>Modelo</b></label>
					<div class="col-sm-9">
						<input maxlength="50" type="text" class="form-control" id="txt_modelo" name="txt_modelo" placeholder="Digite el nombre del modelo" autocomplete="off" required>
					</div>
				</div>

				<div class="col-sm-12 text-right">
					<button type="button" class="btn btn-lg" onclick="habilitarPanel('listado_configuraciones'); resetForm(this.form.id);">Cancelar</button>
					<button type="submit" class="btn btn-info btn-lg" title="Actualizar Modelo"><i class="fas fa-sync-alt"></i> Actualizar</button>
				</div>

			</form>
		</div>

	</div>


	<form action="servidor/controlador.php" method="post" id="FORM_CARGAR_LISTADOS_CONFIGURACION">
		<input type="hidden" value="cargarListadoConfiguraciones" name="id_formulario">
	</form>


</body>

<!-- Permite cargar la lista de configuraciones -->
<script type="text/javascript">

$( document ).ready(function() {
	habilitarPanel('listado_configuraciones');
	cargarListadoConfiguraciones('');
});


// Permite habilitar panel de configuraciones
function habilitarPanel(id_panel){
	$('#listado_configuraciones').hide();
	$('#registrar_estado').hide();
	$('#editar_estado').hide();
	$('#registrar_modelo').hide();
	$('#editar_modelo').hide();
	$('#'+ id_panel).show();
}

// Permite cargar los estados
function cargarEditarEstado(id_estado, estado, estado_inicial){
	id_form = 'FORM_EDITAR_ESTADO';
	$('#'+id_form +' #id_estado').val(id_estado);
	$('#'+id_form +' #txt_estado').val(estado);
	$('#'+id_form + ' #cbxe_estado_inicial').prop( 'checked', (estado_inicial ? true : false) );
	habilitarPanel('editar_estado');
}

// Permite cargar los modelos registrados.
function cargarEditarModelo(id_modelo, modelo){
	id_form = 'FORM_EDITAR_MODELO';
	$('#'+id_form +' #id_modelo').val(id_modelo);
	$('#'+id_form +' #txt_modelo').val(modelo);
	habilitarPanel('editar_modelo');
}

// Permite editar el estado del proyecto.
function editarEstado(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false,  data.msg , null);
			} else {
				alertSucess(false, data.msg);
				habilitarPanel('listado_configuraciones');
				resetForm(id_form);
				cargarListadoConfiguraciones();
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}


// Permite editar el modelo del proyecto.
function editarModelo(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false,  data.msg , null);
			} else {
				alertSucess(false, data.msg);
				habilitarPanel('listado_configuraciones');
				resetForm(id_form);
				cargarListadoConfiguraciones();
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}

// Permite registrar nuevos estados para los proyectos.
function registrarEstado(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false,  data.msg , null);
			} else {
				alertSucess(false, data.msg);
				habilitarPanel('listado_configuraciones');
				resetForm(id_form);
				cargarListadoConfiguraciones();
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}

// Permite registrar nuevos modelos para los proyectos.
function registrarModelo(id_form, event){
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
			spinnerShow();
		},
		success: function(data){
			if (data.status === 0) {
				alertDanger(false,  data.msg , null);
			} else {
				alertSucess(false, data.msg);
				habilitarPanel('listado_configuraciones');
				resetForm(id_form);
				cargarListadoConfiguraciones();
			}
			spinnerHidden2();
		},
		error: function(data){
			console.log(data.responseText);
		}
	};

	if (validateFormById(id_form)) {
		$('#' + id_form).ajaxSubmit(options);				
	}
	event.preventDefault();
	return false;
}

// Permite cargar las listas de los ESTADOS y MODELOS de los proyectos.
function cargarListadoConfiguraciones(){
	id_form = 'FORM_CARGAR_LISTADOS_CONFIGURACION';
	var options = {
		dataType: 'json',
		beforeSubmit: function () {
				//spinnerShow();
			},
			success: function(data){
				if (data.status === 0) {
					alertDanger(false,  data.msg);
				} else {
					list = $("#TBODY_LISTADO_ESTADOS");
					list.empty();
					for (i = 0; i < data.estados.length; i++) {
						e = data.estados[i];

						estado_inicial = '';
						if(e.estado_inicial == 1){
							estado_inicial = " <small>(Inicial)</small>"
						}

						child = '<tr>'
						+ '<th scope="row">' + e.estado + estado_inicial + '</th>'
						+ '<td>' 
						+ '<button class="btn btn-warning" onclick="cargarEditarEstado(' + e.id_estado + ', \'' + e.estado + '\',' + e.estado_inicial +')" title="Editar"><i class="far fa-edit"></i></button> '
						+ '</td>'
						+ '</tr>';
						list.append(child);
					}


					list = $("#TBODY_LISTADO_MODELOS");
					list.empty();
					for (i = 0; i < data.modelos.length; i++) {
						t = data.modelos[i];
						child = '<tr>'
						+ '<th scope="row">' + t.modelo + '</th>'
						+ '<td>' 
						+ '<button class="btn btn-warning" onclick="cargarEditarModelo(' + t.id_modelo + ', \'' + t.modelo + '\');" title="Editar"><i class="far fa-edit"></i></button> '
						+ '</td>'
						+ '</tr>';
						list.append(child);
					}
				}
				spinnerHidden2();
			},
			error: function(data){
				console.log(data.responseText);
			}
		};
		$('#' + id_form).ajaxSubmit(options);
		return false;
	}
	</script>
	</html>