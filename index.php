<?php 
  require_once dirname(__FILE__) . '/servidor/controlador.php';
?>

<!DOCTYPE html>
<html lang="es">

  <head>

    <title>Inicio</title>
    <?php include 'head.php'; ?>
    
  </head>

  <body id="page-top">

    <?php include 'nav.php'; ?>

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center" style="padding-top: 25%;height: 100%;">
      <div class="container">

        <!--<img class="img-fluid mb-5 d-block mx-auto" src="recursos/img/control_proyectos_main.png" alt="">-->
        <!-- Mensaje de Bienvenida -->
        <h1>Control de Proyectos.</h1>
        <hr style="background:white;">
        <h2 class="font-weight-light mb-0">Bienvenido</h2>
      </div>
    </header>


    <!-- Botón superior (solo visible en tamaños de pantalla pequeños y extra pequeños) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>


  </body>

</html>
