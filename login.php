<!-- Clase que permite la visualizacion y validacion del LOGIN-->
<?php
require_once dirname(__FILE__) . '/servidor/controlador.php';
if (Controlador::usuarioEstaLogueado()) {
    header("Location: ./");
}

?>

<!DOCTYPE html>
<html lang="es">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
	<!-- Nombre en pestaña -->
	<title>Ingresar</title>
	<?php include 'head.php'; ?>

</head>

<body id="page-top">

	<?php include 'nav.php'; ?>
	<section></section>

	<!-- Formulario de INICIO DE SESION -->
	<div class="col-sm-4 offset-sm-4">
		<div class="jumbotron">
			<form id="frmLogin" action="servidor/controlador.php" class="form-horizontal" method="post" onsubmit="loginAjax(event);">
				<input type="hidden" value="login" name="id_formulario">
				<h2 class="text-warning text-center">Iniciar Sesión</h2>
				<hr>
				<div class="form-group">

					<!-- Campo de nombre de usuario-->
					<input class="col-12 form-control form-control-lg" type="text" name="username" id="username" required="true" placeholder="Nombre de usuario">
				</div>
				<div class="form-group">

					<!-- Campo de contraseña de usuario-->
					<input class="col-12 form-control form-control-lg" type="password" name="password" id="password" required="true" placeholder="Contraseña">
				</div>
				<hr>

				<!-- Boton inicio de sesion-->
				<button class="btn btn-success offset-sm-2 col-8" type="submit">Ingresar</button>

				<div id="msg" class="col-sm-12 text-center text-danger py-2"></div>

			</form>

		</div>
	</div>
</body>


<!-- Muestra la respuesta del LOGIN -->
<script>
	function loginAjax(event) {
		var options = {
			beforeSubmit: function () {
				spinnerShow();
			},
			success: mostrarRespuestaLogin
		};

		if (validarFormulario('frmLogin')) {
			$('#frmLogin').ajaxSubmit(options);
		}
		event.preventDefault();
		return false;
	}

	
	function mostrarRespuestaLogin(responseText, statusText, xhr, $form) {
		spinnerHidden();
		if (responseText === "") {
			//window.location.assign("inicio");
            window.location.assign("/");
        } else {
        	document.getElementById('msg').innerHTML = responseText;
        }
    }

	// Validacion de la respuesta del LOGIN 
    function validarFormulario(idForm) {
    	return document.getElementById(idForm).checkValidity();
    }

</script>
</html>