 <!-- MENU DE NAVEGACION -->
 <?php 
 require_once dirname(__FILE__) . '/servidor/controlador.php'; 
 ?>
 <?php include 'spinner.php'; ?>

<!-- Logo en la parte lateral -->
 <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav" style="padding: 0; background-color: #d4dae0 !important;">
  <a href="./" style="margin-left: 3%;">
      <img class="navbar-brand js-scroll-trigger" src="recursos/img/logo.jpeg" width="50">
    </a>
  <div class="container">

    <!-- Barra de navegacion -->
    <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fa fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">


		<!-- Tipo de usuario logueado -->
        <?php if (Controlador::usuarioEstaLogueado()) { ?>

          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded" href="proyectos.php">Proyectos</a>
          </li>

          <!-- Otorga permisos de usuario logueado -->
          <?php if (Controlador::darNivelPermiso() == Controlador::$NIVEL_ADMINISTRADOR) { ?>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded" href="usuarios.php">Usuarios</a>
            </li>

            <!-- Muestra las configuraciones -->
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded" href="configuracion.php">Configuraciones</a>
            </li>
          <?php } ?>
        <?php } ?>

        <!-- Devuleve informacion del usuario loqueado -->
        <?php if (Controlador::usuarioEstaLogueado()) { ?>
          <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-3 px-0 px-lg-3 rounded text-warning" style="font-size: 12px; width: 300px;" href="perfil.php"><small><?php echo Controlador::darInformacionUsuario(); ?></small></a>
          </li>
        <?php } ?>

        <!-- Error de mensaje no encontrado -->
        <li class="nav-item mx-0 mx-lg-1" style="margin-left: 10% !important; width: 180px;">
          <?php if (!Controlador::usuarioEstaLogueado()) { ?>
          <a class="nav-link py-3 px-0 px-lg-3 rounded text-warning" href="login.php"><i class="fas fa-sign-in-alt"></i> Ingresar</a>
          <?php }else{?>
          <form action="servidor/controlador.php" method="post" class="navbar-btn">
            <input type="hidden" value="unlogin" name="id_formulario">
            <button type="submit" class="btn btn-danger" title="Salir" style="margin: 5px;"><i class="fas fa-sign-out-alt"></i></button>
          </form>

          <?php } ?>
        </li>
      </ul>
    </div>
  </div>
</nav>