
<?php if (Controlador::usuarioEstaLogueado()) { ?>
<ul class="sidebar navbar-nav">

	<!-- Retorna el nivel de permiso del usuario -->
    <li class="nav-item active">
      <a class="nav-link" style="font-size: 18px;">
        <span><small><b><?php echo Controlador::darInformacionUsuario(); ?></b></small></span></a>
    </li>
    

    <li class="nav-item">
      <a class="nav-link" href="proyectos.php">
        <i class="fa fa-tasks"></i>
        <span>Proyectos</span>
      </a>
    </li>

    <!-- Muestra pestaña USUARIOS y CONFIGURACIONES si el usuario es ADMINISTRADOR -->
    <?php if (Controlador::darNivelPermiso() == Controlador::$NIVEL_ADMINISTRADOR) { ?>

    <!-- Panel de USUARIOS -->
    <li class="nav-item">
      <a class="nav-link" href="usuarios.php">
        <i class="fa fa-users"></i>
        <span>Usuarios</span></a>
      </li>

      <!-- Panel de CONFIGURACIONES -->
      <li class="nav-item">
        <a class="nav-link" href="configuracion.php">
          <i class="fa fa-cog"></i>
          <span>Configuraciones</span></a>
        </li>

    <?php } ?>


      </ul>

<?php } ?>