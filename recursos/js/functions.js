var maxFiles = 1;
var maxSize = 10000000;
var files = [];
var typesF = ["image/png","image/x-png", "image/jpeg", "application/pdf", "application/x-zip-compressed", "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"];
var miliseconds = 0;

Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
};

Array.prototype.init = function (size, value) {
    while (size--) {
        this[size] = value;
    }
};
initFilesLocal();
function initFilesLocal() {
    files.init(maxFiles, null);
}

function validateFile(element) {
    var msg = null;
    for (i = 0; i < element.files.length; i++) {
        var oFile = element.files[i];
        //console.log(oFile.type + " - " + oFile.size + " - " + typesF.indexOf(oFile.type) + " - " + element.files.length);
        if (oFile.size > 1000000) {
            msg = "Cada archivo no debe superar los " + (maxSize / 1000000) + "mb.";
        }

        if (!(typesF.indexOf(oFile.type) > -1)) {
            msg = "Los archivos deben ser de tipo imagen, pdf o .zip .";
        }
    }

    if (msg !== null) {
        alert(msg);
        element.value = "";
        return false;
    }
    return true;
}

function validate(element) {
    if (!validateFile(element)) {
        return;
    }
}

function deleteFile(value) {
    for (i = 0; i < files.length; i++) {
        if (files[i] !== null) {
            file = files[i];
            if (file.name === value) {
                files[i] = null;
                var table = document.getElementById('tableFiles');
                var tbody = table.getElementsByTagName('tbody')[0];
                var tr = table.getElementsByClassName(value)[0];
                tbody.removeChild(tr);
                var inputFile = document.getElementById('inputFile');
                inputFile.disabled = false;
                return;
            }
        }
    }
}

function removeAllFiles() {
    for (i = 0; i < files.length; i++) {
        if (files[i] !== null) {
            file = files[i];
            files[i] = null;
            var table = document.getElementById('tableFiles');
            var tbody = table.getElementsByTagName('tbody')[0];
            var tr = table.getElementsByClassName(file.name)[0];
            tbody.removeChild(tr);
            var inputFile = document.getElementById('inputFile');
            inputFile.disabled = false;
        }
    }
    initFilesLocal();
}

function countTextArea(id, idMsg) {
    id = '#' + id;
    var text_max = $(id).attr('maxlength');
    var text_length = $(id).val().length;
    idMsg = '#' + idMsg;
    $(idMsg).html('(Maxímo ' + text_max + ' caracteres) Faltan ' + (text_max - text_length) + ' caracteres');
}

function countTextArea(textArea) {
    var text_max = textArea.maxLength;
    var text_length = textArea.textLength;
    msg = textArea.nextElementSibling;
    msg.innerHTML = '(Maxímo ' + text_max + ' caracteres) Faltan ' + (text_max - text_length) + ' caracteres';
}

function writeFilesInForm(idForm) {
    var addFiles = 0;
    var form = document.getElementById(idForm);
    for (i = 0; i < files.length; i++) {
        if (files[i] !== null) {
            form.appendChild(files[i].input);
            addFiles++;
        }
    }
    return addFiles;
}

function loadTypesPerm(input) {
    for (i = 0; i < typesF.length; i++) {
        input.accept += typesF[i] + ((i + 1) < typesF.length ? ',' : '');
    }
}

function validateFormById(idForm) {
    return document.getElementById(idForm).checkValidity();
}

function resetForm(idForm) {
    document.getElementById(idForm).reset();
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function loadSelectWhitInfo(idSelect, idInfo) {
    $('#' + idSelect).change(function () {
        var $selected = $(this).find(':selected');
        var description = $selected.data(idInfo);
        $("#" + idInfo).css("visibility", (isBlank(description) ? "hidden" : "inherit"));
        $("#" + idInfo).html(description);
    }).trigger('change');
}

function spinnerHidden() {
    var milisecondsActual = new Date().getTime();
    var showIn = 300;
    miliseconds = milisecondsActual - miliseconds;
    showIn = showIn - miliseconds;
    setTimeout(function () {
        var spinner = document.getElementById("spinner_gif");
        spinner.style.visibility = 'hidden';
    }, showIn);
}

function spinnerHidden2() {
    var milisecondsActual = new Date().getTime();
    var showIn = 300;
    miliseconds = milisecondsActual - miliseconds;
    showIn = showIn - miliseconds;
    setTimeout(function () {
        var spinner = document.getElementById("spinner_gif");
        spinner.style.visibility = 'hidden';
    }, showIn);
}

function spinnerShow() {
    miliseconds = new Date().getTime();
    var spinner = document.getElementById("spinner_gif");
    spinner.style.visibility = 'inherit';
}

function action_required(idDiv, idElement, has_required) {
    document.getElementById(idElement).required = has_required;
    if (has_required) {
        $("#" + idDiv).addClass("required");
    } else {
        $("#" + idDiv).removeClass("required");
    }
}

function changeIcon(ele) {
    span = ele.lastElementChild;
    actual = span.className;
    span.className = actual == 'glyphicon glyphicon-triangle-bottom' ? 'glyphicon glyphicon-triangle-top' : 'glyphicon glyphicon-triangle-bottom';
}

function getMsgMaxFile(idMsgMaxFiles) {
    msg = 'Tipo archivo permitido (word, imagen y pdf). Tamaño maximo ' + (maxSize / 1000000) + 'MB';
    document.getElementById(idMsgMaxFiles).innerHTML = msg;
}

function keyHiddenMdl(e, idMdl) {
    key = (document.all) ? e.keyCode : e.which;
    if (key == 13) {
        $('#' + idMdl).modal('hide');
        return 0;
    }
}

function copyToClipBoard(idBoxText) {
    inp = document.getElementById(idBoxText);
    inp.select();
    document.execCommand("copy");
}

function isEmpty(value) {
    return (value === null || value === "" || value === '' || value.length === 0);
}

function generatorPassword(size) {
    var upperCase = "ABCDEFGHIJKLMNPQRTUVWXYZ";
    var lowerCase = "abcdefghijkmnpqrtuvwxyz";
    var numbers = "12346789";
    var characteres = lowerCase + upperCase + numbers;
    var password = "";
    password += upperCase.charAt(Math.floor(Math.random() * upperCase.length));
    password += lowerCase.charAt(Math.floor(Math.random() * lowerCase.length));
    password += numbers.charAt(Math.floor(Math.random() * numbers.length));
    max = (size - password.length);
    for (i = 0; i < max; i++) {
        password += characteres.charAt(Math.floor(Math.random() * characteres.length));
    }
    return password;
}

function alertWarning(hidden, msg) {

    $('#myModalSuccess').modal('show');
    $('#myModalSuccess').find('#tituloDialogo').html('Alerta');
    $('#myModalSuccess').find('#content').html(msg);


    /*if (hidden) {
        divs = $('.alertWarning');
        for(i = 0 ; i<  divs.length ; i++){
            $(divs[i]).attr('hidden', true);
        }
    } else {
        divs = $('.alertWarning');
        for(i = 0 ; i <  divs.length ; i++){
            $(divs[i]).attr('hidden', false);
            $(divs[i]).find('#msg').html('<strong>Alerta! </strong> ' + msg);
        }
        setTimeout(function () {
            alertWarning(true, null);
        }, 3000);
        
    }*/
}

function alertSucess(hidden, msg) {

    $('#myModalSuccess').modal('show');
    $('#myModalSuccess').find('#tituloDialogo').html('Información');
    $('#myModalSuccess').find('#content').html(msg);
    

    /*if (hidden) {
        divs = $('.alertSuccess');
        for(i = 0 ; i<  divs.length ; i++){
            $(divs[i]).attr('hidden', true);
        }
    } else {

        divs = $('.alertSuccess');
        for(i = 0 ; i <  divs.length ; i++){
            $(divs[i]).attr('hidden', false);
            $(divs[i]).find('#msg').html(msg);
        }
        setTimeout(function () {
            alertSucess(true, null);
        }, 3000);
        
    }*/
}

function alertDanger(hidden, msg, idAlert) {
    //idAlert = idAlert ? '#'+idAlert : '.alertDanger';
    /*if (hidden) {
        $(idAlert).attr('hidden', true);
    } else {
        $(idAlert).attr('hidden', false);
        $(idAlert +' #msg').html(msg);
        setTimeout(function () {
            alertDanger(true, null, null);
        }, 3000);
    }*/

    /*if (hidden) {
        divs = $(idAlert);
        for(i = 0 ; i<  divs.length ; i++){
            $(divs[i]).attr('hidden', true);
        }
    } else {
        divs = $(idAlert);
        for(i = 0 ; i <  divs.length ; i++){
            $(divs[i]).attr('hidden', false);
            $(divs[i]).find('#msg').html(msg);
        }
        setTimeout(function () {
             alertDanger(true, null, null);
        }, 3000);
        
    }*/


    $('#myModalSuccess').modal('show');
    $('#myModalSuccess').find('#tituloDialogo').html('Error');
    $('#myModalSuccess').find('#content').html(msg);
}

function clearChilds(fater) {
    isFater = false;
    while (fater.firstChild) {
        fater.removeChild(fater.firstChild);
        isFater = true;
    }
    return isFater;
}

onerror = handleErr;

function handleErr(msg, url, l) {
    txt = "Error: " + msg + "\n";
    txt += "url: " + url + "\n";
    txt += "line: " + l + "\n";
    alert(txt);
    console.log(txt);
    return true;
}